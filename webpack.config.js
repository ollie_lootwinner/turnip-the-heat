var HtmlWebpackPlugin = require('./slot-base/node_modules/html-webpack-plugin');
var CopyWebpackPlugin = require('./slot-base/node_modules/copy-webpack-plugin');

const path = require('path');

module.exports = {

  entry: {
    app: [
      path.join(__dirname, '/slot-base/lib/pixi.min.js'),
      path.join(__dirname, '/slot-base/lib/phaser.min.js'),
      path.join(__dirname, '/slot-base/src/index.ts')
    ]
  },

  output: {
    path: path.join(__dirname, '/dist'),
    filename: '[name].js',
    chunkFilename: '[name].js',
  },

  resolve: {
    extensions: ['.ts', '.js', 'json'],
  },

  plugins: [
    new HtmlWebpackPlugin({
      title: "Turn'p the Heat!",
      template: path.join(__dirname, '/slot-base/index.template.ejs')
    }),
    new CopyWebpackPlugin([
      { from: 'assets', to: './assets' },
      { from: '../usedAssets/assets', to: './assets' },
      { from: '../config', to: './' },
      { from: 'styles', to: './' },
    ])
  ],

  // devtool: 'source-map',
  performance: { hints: false },
  mode: "production",

  module: {
    rules: [
      { test: /pixi\.min\.js/, use: [{ loader: 'expose-loader', options: 'PIXI' }] },
      { test: /phaser\.min\.js/, use: [{ loader: 'expose-loader', options: 'Phaser' }] },
      { test: /\.ts$/, exclude: /node_modules/, use: ['babel-loader', 'ts-loader'] },
      { test: /\.(png|jpg|gif)$/, loader: 'url-loader' }
    ]
  }
};