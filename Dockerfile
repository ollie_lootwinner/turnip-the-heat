FROM gcr.io/cashed/nginx-resolve:2019-05-23

COPY dist/ $WWWROOT
RUN rm $WWWROOT/deploy-config.json
COPY dist/deploy-config-template.json $WWWROOT/deploy-config.json