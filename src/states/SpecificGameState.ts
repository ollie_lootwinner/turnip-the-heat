import { Config } from "../../slot-base/src/Config";
import { AssetLoader } from "../../slot-base/src/components/AssetLoader";
import { SoundManager } from "../../slot-base/src/components/SoundManager";
import { SignalDispatcher } from "../../slot-base/src/components/SignalDispatcher";
import { MiscUtil } from "../../slot-base/src/util/Misc";
import { MathUtil } from "../../slot-base/src/util/Math";
import { BetResult } from "../components/BetResult";
import { BetUI, Bet } from "../components/BetUI";
import { Panel } from "../components/Panel";
import { Player } from "../components/PlayerPanel";
import { ResultsDialog } from "../dialogs/ResultsDialog";
import { GameStartDialog } from "../dialogs/GameStartDialog";
import { Sound } from "phaser-ce";

export class SpecificGameState extends Phaser.State {

	private _bounds: Phaser.Rectangle;
	private _bg: Phaser.Image;
	protected _instructions: Phaser.BitmapText;
	private _gridLayer: Phaser.Group;
	private _turnipLayer: Phaser.Group;
	private _grid: Panel[];
	private _ui: BetUI;

	private _forcedWin: boolean;
	private _result: BetResult;

	public preload(p_game: Phaser.Game): void {
		let image: Phaser.Image = new Phaser.Image(p_game, 0, 0, "game/loading");
		image.anchor.set(0.5);
		image.x = this.game.width * 0.5;
		image.y = this.game.height * 0.5;
		this.game.world.addChild(image);

		p_game.add.tween(image)
		.to({ y: p_game.height * 0.47 }, 600, Phaser.Easing.Linear.None, true, 0, -1, true);

		AssetLoader.load(this.game, Config.instance.assets.general);
		super.preload(p_game);
	}

	public async create(p_game: Phaser.Game): Promise<void> {
		this.game.cache.addBitmapFontFromAtlas("fonts/font_standard", "game/background_texture", "font_standard", "font_standard_xml");
		super.create(p_game);

		p_game.renderer.setTexturePriority([
			"game/background_texture"
		]);

		let params: URLSearchParams = new URLSearchParams(location.search.substring(1));
		if (params.has("forceWin")) {
			this._forcedWin = params.get("forceWin") === "true";
		}

		SoundManager.instance.init(this.game, false, false, false);
		SoundManager.instance.playMusic("sfx/music", 1, true);

		this._bounds = new Phaser.Rectangle(0, 0, this.game.width, this.game.height);

		this._bg = new Phaser.Image(this.game, 0, 0, "game/background_texture", "bg_panel_2");
		this._bg.anchor.set(0, 0.5);
		this._bg.x = this._bounds.width * 0.0;
		this._bg.y = this._bounds.height * 0.505;
		this.game.world.addChild(this._bg);

		this._instructions = new Phaser.BitmapText(
			this.game, 0, 0, "fonts/font_standard", "CHOOSE 1-8 SPOTS ON THE BOARD TO PLAY!", 48, "center",
		);
		this._instructions.anchor.set(0.5, 0.5);
		this._instructions.x = this._bounds.width * 0.45;
		this._instructions.y = this._bounds.height * 0.035;
		this.game.world.addChild(this._instructions);

		this._gridLayer = new Phaser.Group(this.game);
		this.game.world.addChild(this._gridLayer);
		this._turnipLayer = new Phaser.Group(this.game);
		this.game.world.addChild(this._turnipLayer);

		this._grid = [];
		for (let i=0; i < 18; i++) {
			let panel: Panel = new Panel(this.game, this._gridLayer);
			panel.init(this._bounds, i, this._turnipLayer, (pnl: Panel, pntr: Phaser.Pointer) => {
				this._ui.onPanelTapped(pnl, pntr);
			});
			this._grid.push(panel);
		}

		this._ui = new BetUI(this.game);
		this._ui.init(this._bounds);
		SignalDispatcher.spinButtonClickedSignal.add( (total: number, bets: Bet[]) => {
			this.playGame(total, bets);
		});
		this.game.world.addChild(this._ui);
	}

	private async playGame(totalAmount: number, bets: Bet[]) {
		this._ui.gamestate.updateState("OTHER\nPLAYERS\nBETTING");
		this._ui.lockUI();
		this._instructions.visible = false;

		await MiscUtil.timeoutPromise(this.game, 500);

		let compPlayerBets: Bet[][] =[];
		for (let i=0; i < 2; i++) {
			compPlayerBets.push([]);
			let numBets: number = Math.floor(Math.random() * 5) + 4;
			let betSpaces: number[] = [];
			for (let i = 1; i <= this._grid.length; i++) {
				betSpaces.push(i);
			}
			for (let j=0; j < numBets; j++) {
				let space: number = betSpaces.splice(Math.floor(Math.random() * betSpaces.length), 1)[0];
				compPlayerBets[i].push( new Bet(200, space) );
				this._grid[space - 1].setSelected(true, i + 1);
				this._ui.players.updatePlayerTag( new Player(i + 1, compPlayerBets[i]) );
				await MiscUtil.timeoutPromise(this.game, 250 + Math.floor(Math.random() * 500));
			}
		}

		this._ui.gamestate.updateState("STARTS\nIN 10");
		await MiscUtil.timeoutPromise(this.game, 1000);
		this._ui.gamestate.updateState("STARTS\nIN 9");
		await MiscUtil.timeoutPromise(this.game, 1000);
		this._ui.gamestate.updateState("STARTS\nIN 8");
		await MiscUtil.timeoutPromise(this.game, 1000);
		this._ui.gamestate.updateState("STARTS\nIN 7");
		await MiscUtil.timeoutPromise(this.game, 1000);
		this._ui.gamestate.updateState("STARTS\nIN 6");
		await MiscUtil.timeoutPromise(this.game, 1000);
		this._ui.gamestate.updateState("STARTS\nIN 5");
		await MiscUtil.timeoutPromise(this.game, 1000);
		this._ui.gamestate.updateState("STARTS\nIN 4");
		await MiscUtil.timeoutPromise(this.game, 1000);
		this._ui.gamestate.updateState("STARTS\nIN 3");
		await MiscUtil.timeoutPromise(this.game, 1000);
		this._ui.gamestate.updateState("STARTS\nIN 2");
		await MiscUtil.timeoutPromise(this.game, 1000);
		this._ui.gamestate.updateState("STARTS\nIN 1");

		SoundManager.instance.crossFadeMusic("sfx/music_session", 5500);
		await MiscUtil.timeoutPromise(this.game, 1000);
		this._ui.gamestate.updateState("STARTS\nIN 0");

		let gameStart = new GameStartDialog(this.game, this._bounds);
		this.game.world.addChild(gameStart);
		await MiscUtil.timeoutPromise(this.game, 4000);
		gameStart.destroy();

		await MiscUtil.timeoutPromise(this.game, 1500);
		this._ui.gamestate.updateState("GAME\nIN\nSESSION");

		let allValues: number[] = [];
		for (let i = 1; i <= this._grid.length; i++) {
			allValues.push(i);
		}
		allValues = MathUtil.shuffle(allValues);

		this._result = new BetResult(bets, this._forcedWin);

		let survivors: number[] = this._result.winningSpots.slice(0);

		let skippedForAnticipation: number[] = [];
		for (let i=0; i < allValues.length; i++) {
			if (survivors.indexOf(allValues[i]) < 0) {

				// Don't reveal chosen numbers too early
				if (this._grid[allValues[i] - 1].selected && Math.random() > (0.4 + i / allValues.length) ) {
					skippedForAnticipation.push(allValues[i]);
					continue;
				}

				this._grid[allValues[i] - 1].reveal();
				this.findNeighbours(this._grid[allValues[i] - 1]).forEach( (p: Panel) => p.cower() );

				await MiscUtil.timeoutPromise(this.game, Math.random() * 1500);
			}
		}

		// Now do the anticipated ones
		for (let i=0; i < skippedForAnticipation.length; i++) {
			this._grid[skippedForAnticipation[i] - 1].reveal();
			await MiscUtil.timeoutPromise(this.game, Math.random() * 1500);
		}

		SoundManager.instance.crossFadeMusic("sfx/music", 1000);

		this._ui.gamestate.updateState("GAME\nRESULTS");

		let lastAnimation: Promise<void>;
		for (let i=0; i < survivors.length; i++) {
			lastAnimation = this._grid[survivors[i] - 1].animate(this._result.winMultipliers[i], i * 500);
		}
		await lastAnimation;

		let winTotal: number = 0;
		for (let i=0; i < bets.length; i++) {
			let index: number = this._result.winningSpots.indexOf(bets[i].location);
			if (index >= 0) {
				winTotal += bets[i].amount * this._result.winMultipliers[index];
				console.log(`Bet[${i}] Won: ${bets[i].amount * this._result.winMultipliers[index]}`);
			}
		}
		console.log(`Total Won: ${winTotal}`);

		let compWins: number[] = [0, 0];
		for (let j=0; j < 2; j++) {
			for (let i=0; i < compPlayerBets[j].length; i++) {
				let index: number = this._result.winningSpots.indexOf(compPlayerBets[j][i].location);
				if (index >= 0) {
					compWins[j] += compPlayerBets[j][i].amount * this._result.winMultipliers[index];
				}
			}
		}

		await MiscUtil.timeoutPromise(this.game, 1500);

		await new Promise<void>((resolve) => {
			let results: ResultsDialog = new ResultsDialog(this.game, this._bounds, [winTotal, compWins[0], compWins[1]]);
			results.onClose.addOnce(() => resolve());
			this.game.world.addChild(results);
		});

		await MiscUtil.timeoutPromise(this.game, 2000);

		for (let i=0; i < this._grid.length; i++) {
			this._grid[i].resetToStart();
		}
		this._ui.onGameOver();
		this._instructions.visible = true;
	}

	private findNeighbours(panel: Panel): Panel[] {
		let neighbours: Panel[] = [];

		for (let j=0; j < this._grid.length; j++) {
			if (j !== (panel.value - 1)) {
				let dist: number = panel.currentPos.distance(this._grid[j].currentPos);
				if (dist < 420) {
					neighbours.push(this._grid[j]);
				}
			}
		}
		return neighbours;
	}
}