import { App } from "../../slot-base/src/App";
import { BettingConfig } from "../../slot-base/src/reels/BettingConfig";
import { PaylinesConfig } from "../../slot-base/src/reels/PaylinesConfig";
import { AssetLoader } from "../../slot-base/src/components/AssetLoader";
import { Config } from "../../slot-base/src/Config";
export class LoadState extends Phaser.State {

	public preload(p_game: Phaser.Game): void {
		AssetLoader.load(this.game, Config.instance.assets.loading);
	}

	public create(p_game: Phaser.Game): void {

		// Pick 1 - 8 spots
		PaylinesConfig.lines = [ [1], [1], [1], [1], [1], [1], [1], [1] ];
		BettingConfig.BET_CHANGE_AMOUNT = 200;

		p_game.state.start("SpecificGameState", true, false);
	}
}