import { MiscUtil } from "../../slot-base/src/util/Misc";

export class UrlConfig {
	public static debugMode: boolean = false;
	public static DEBUG_MODE_PARAM: string = "d";
	public static bigWinForce: boolean = false;
	public static BIG_WIN_FORCE_PARAM: string = "b";
	public static FORCE_FREE_SPIN_RESULTS: string = "r";
	public static forceFreeSpinMode: number = 0;

	public static AMOUNT_OF_FREESPINS_VS_MINIGAME: number = 0.15;
	public static AMOUNT_OF_FREESPINS_VS_MINIGAME_DURING_FREESPINS: number = -0.1;

	public static initParams() {
		let _params = MiscUtil.getQueryParams();

		if (UrlConfig.DEBUG_MODE_PARAM in _params) {
			UrlConfig.debugMode = _params[UrlConfig.DEBUG_MODE_PARAM][0] === "1";
		}
		if (this.BIG_WIN_FORCE_PARAM in _params) {
			UrlConfig.bigWinForce = _params[UrlConfig.BIG_WIN_FORCE_PARAM][0] === "1";
		}
		if (UrlConfig.FORCE_FREE_SPIN_RESULTS in _params) {
			UrlConfig.forceFreeSpinMode = parseInt(_params[UrlConfig.FORCE_FREE_SPIN_RESULTS][0]);
		}
	}
}