import { AssetLoader } from "../../slot-base/src/components/AssetLoader";
import { Config } from "../../slot-base/src/Config";

export function loadGameStateResources(p_game: Phaser.Game) {
	AssetLoader.load(p_game, Config.instance.assets.general);
}