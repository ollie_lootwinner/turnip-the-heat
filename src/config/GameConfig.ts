import { TableName } from "../../slot-base/src/slotMath/paytables/Paytable";

export class GameConfig {
	public static GAME_NAME: string = "Gem Booster";
	public static PAYTABLE_NAME: TableName = TableName.GEM_BOOSTER_LEVEL_ONE;
	public static PAYTABLE_NAME_FREESPINS: TableName = TableName.GEM_BOOSTER_LEVEL_ONE;
	public static IDLE_STRINGS: string[] = ["LEVEL UP GEMS FOR MULTIPLIERS!"];
	public static MAX_FREESPIN_ICONS: number = 3;
	public static MAX_SCATTER_ICONS: number = 3;

	public static PAYLINE_HIGHLIGHT_DURATION_MS: number = 1000;
	public static PAYLINE_AMOUNT_FADE_TWEEN_DURATION: number = 300;
	public static SLOT_SPIN_DURATION_MS: number = 500;

	public static GAME_MUSIC_VOLUME: number = 0.8;
	public static FREESPIN_MUSIC_VOLUME: number = 0.8;
	public static FEATURE_MUSIC_VOLUME: number = 0.8;

	public static BIG_WIN_THRESHOLD_MULTI: number = 6.0;
	public static BIG_WIN_TIER_2_THRESHOLD_MULTI: number = 10.0;
	public static BIG_WIN_TIER_3_THRESHOLD_MULTI: number = 25.0;

	public static SCATTER_THRESHOLD_MULTI: number = 8.0;
	public static SCATTER_TIER_2_THRESHOLD_MULTI: number = 15.0;
	public static SCATTER_TIER_3_THRESHOLD_MULTI: number = 25.0;

	public static FREESPIN_THRESHOLD_MULTI: number = 8.0;
	public static FREESPIN_TIER_2_THRESHOLD_MULTI: number = 15.0;
	public static FREESPIN_TIER_3_THRESHOLD_MULTI: number = 25.0;

	public static NUM_BIG_WIN_SPARKLES: number = 8;

	public static BIG_WIN_TEXT_COUNTUP_DURATION_MS_1: number = 6000;
	public static BIG_WIN_TEXT_COUNTUP_DURATION_MS_2: number = 12000;
	public static BIG_WIN_TEXT_COUNTUP_DURATION_MS_3: number = 20000;

	public static WIN_TEXT_COUNTUP_DURATION_MS: number = 1000;
	public static GOLDEN_COIN_COUNTUP_DURATION_MS: number = 500;

	public static BUTTON_HOLD_DURATION: number = 500;
	public static AUTOSPIN_INCREMENTS: number[] = [10, 20, 50, 100];
	public static FREESPIN_AMOUNTS: number[] = [10, 10, 10];
}