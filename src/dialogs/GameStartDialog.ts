import { MiscUtil } from "../../slot-base/src/util/Misc";
import { SoundManager } from "../../slot-base/src/components/SoundManager";

export class GameStartDialog extends Phaser.Group {

	protected _bounds: Phaser.Rectangle;

	protected _backdrop: Phaser.Graphics;
	protected _starburst: Phaser.Image;
	protected _bgGroup: Phaser.Group;
	protected _bg: Phaser.Image;
	protected _introText: Phaser.BitmapText;
	protected _icon: Phaser.Image;

	constructor(game: Phaser.Game, bounds: Phaser.Rectangle) {
		super(game);

		this._bounds = bounds;

		this._backdrop = new Phaser.Graphics(this.game, 0, 0);
		this._backdrop.beginFill(0x000000, 0.5);
		this._backdrop.drawRect(0, 0, this._bounds.width, this._bounds.height);
		this._backdrop.endFill();
		this._backdrop.inputEnabled = true;
		this.addChild(this._backdrop);

		this._starburst = new Phaser.Image(this.game, 0, 0, "game/background_texture", "transition_starburst");
		this._starburst.anchor.set(0.5);
		this._starburst.scale.set(2);
		this._starburst.x = this._bounds.width * 0.5;
		this._starburst.y = this._bounds.height * 0.47;
		this._starburst.visible = false;
		this.addChild(this._starburst);

		this._bgGroup = new Phaser.Group(this.game, this);
		this._bgGroup.x = this._bounds.width * 0.5;
		this._bgGroup.y = this._bounds.height * 0.47;

		this._bg = new Phaser.Image(this.game, 0, 0, "game/background_texture", "transition_badge");
		this._bg.anchor.set(0.5);
		this._bg.scale.set(2);
		this._bg.visible = false;
		this._bgGroup.addChild(this._bg);

		this._icon = new Phaser.Image(this.game, 0, 0, "game/background_texture", "game_title");
		this._icon.anchor.set(0.5);
		this._icon.scale.set(1.1);
		this._icon.y = this._bg.height * -0.03;
		this._icon.visible = false;
		this._bgGroup.addChild(this._icon);

		this._introText = new Phaser.BitmapText(this.game, 0, 0, "fonts/font_standard", "IT'S TIME TO...", 80, "center");
		this._introText.anchor.set(0.5);
		this._introText.scale.set(1.2, 1.2);
		this._introText.x = this._bg.width * -0.125;
		this._introText.y = this._bg.height * -0.21;
		this._introText.angle = -10;
		this._introText.visible = false;
		this._bgGroup.addChild(this._introText);

		this.intro();
	}

	public async intro() {
		let blackOut = this.game.add.tween(this._backdrop)
		.from({ alpha: 0 }, 350, Phaser.Easing.Linear.None, true);

		await MiscUtil.tweenCompletePromise(blackOut);

		this._starburst.visible = true;
		this._bg.visible = true;

		let fadeInBG = this.game.add.tween(this._bg)
		.from({ alpha: 0 }, 500, Phaser.Easing.Linear.None, true)
		.onUpdateCallback((t: Phaser.Tween, percent: number) => {
			this._starburst.alpha = percent;
		});

		await MiscUtil.tweenCompletePromise(fadeInBG);

		SoundManager.instance.playFile("sfx/game_start");

		this._introText.visible = true;
		this.game.add.tween(this._introText.scale)
		.from({ x: 0, y: 0 }, 650, Phaser.Easing.Back.Out, true);

		await MiscUtil.timeoutPromise(this.game, 550);

		this._icon.visible = true;
		let popIn = this.game.add.tween(this._icon.scale)
		.from({ x: 0, y: 0 }, 650, Phaser.Easing.Back.Out, true);

		return MiscUtil.tweenCompletePromise(popIn);
	}

	public update() {
		this._starburst.angle += 0.2;
		this._bg.angle += 0.5;
	}

	public destroy() {
		this.game.add.tween(this._backdrop)
		.to({ alpha: 0 }, 500, Phaser.Easing.Linear.None, true)
		.onComplete.addOnce(() => {
			this._backdrop.inputEnabled = false;
			super.destroy();
		});

		this.game.add.tween(this._bg.scale)
		.to({ x: 0, y: 0 }, 350, Phaser.Easing.Linear.None, true)
		.onUpdateCallback((t: Phaser.Tween, percent: number) => {
			this._introText.scale.set(1 - percent, 1 - percent);
			this._icon.scale.set(1 - percent);
			this._starburst.alpha = 1 - percent;
		});
	}
}