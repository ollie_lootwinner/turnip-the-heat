import { DisablingButton } from "../../slot-base/src/components/DisablingButton";
import { MiscUtil } from "../../slot-base/src/util/Misc";

export class BaseDialog extends Phaser.Group {

	protected _bounds: Phaser.Rectangle;
	protected _backdrop: Phaser.Graphics;
	protected _bgGroup: Phaser.Group;
	protected _bg: Phaser.Image;
	protected _title: Phaser.BitmapText;
	protected _closeButton: DisablingButton;

	public onBackdrop: Phaser.Signal;
	public onClose: Phaser.Signal;

	constructor(game: Phaser.Game, bounds: Phaser.Rectangle, title: string) {
		super(game);

		this._bounds = bounds;
		this.onBackdrop = new Phaser.Signal();
		this.onClose = new Phaser.Signal();

		this._backdrop = new Phaser.Graphics(this.game, 0, 0);
		this._backdrop.beginFill(0x000000, 0.5);
		this._backdrop.drawRect(0, 0, this._bounds.width, this._bounds.height);
		this._backdrop.endFill();
		this._backdrop.inputEnabled = true;
		this._backdrop.events.onInputUp.add(() => {
			this.onBackdrop.dispatch();
		});
		this.addChild(this._backdrop);

		this._bgGroup = new Phaser.Group(this.game, this);
		this._bgGroup.x = this._bounds.width * 0.5;
		this._bgGroup.y = this._bounds.height * 0.47;

		this._bg = new Phaser.Image(this.game, 0, 0, "game/background_texture", "results_panel");
		this._bg.anchor.set(0.5);
		this._bgGroup.addChild(this._bg);

		this._title = new Phaser.BitmapText(this.game, 0, 0, "fonts/font_standard", title, 80, "center");
		this._title.anchor.set(0.5);
		this._title.y = this._bg.height * -0.32;
		this._bgGroup.addChild(this._title);

		this._closeButton = new DisablingButton(
			this.game, 0, 0, "game/background_texture", "close_1", "close_0", "close_2", "close_0", "close_2"
		);
		this._closeButton.anchor.set(0.5);
		this._closeButton.y = this._bg.height * 0.465;
		this._closeButton.onInputUp.add(() => {
			this.onClose.dispatch();
		});
		this._bgGroup.addChild(this._closeButton);
	}

	public async intro() {
		this.game.add.tween(this._backdrop)
		.from({ alpha: 0 }, 350, Phaser.Easing.Linear.None, true);

		let scaleIn = this.game.add.tween(this._bgGroup.scale)
		.from({ x: 0, y: 0 }, 350, Phaser.Easing.Linear.None, true);

		return MiscUtil.tweenCompletePromise(scaleIn);
	}

	public destroy() {
		this._backdrop.inputEnabled = false;
		this._closeButton.inputEnabled = false;

		this.game.add.tween(this._backdrop)
		.to({ alpha: 0 }, 350, Phaser.Easing.Linear.None, true);

		this.game.add.tween(this._bgGroup.scale)
		.to({ x: 0, y: 0 }, 350, Phaser.Easing.Linear.None, true)
		.onComplete.addOnce(() => {
			this.onBackdrop.dispose();
			this.onClose.dispose();
			super.destroy();
		});
	}
}