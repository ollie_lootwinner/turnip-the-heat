import { BaseDialog } from "./BaseDialog";
import { SoundManager } from "../../slot-base/src/components/SoundManager";

export class GameStartDialog extends BaseDialog {

	constructor(game: Phaser.Game, bounds: Phaser.Rectangle) {
		super(game, bounds, "IT'S TIME TO...");

		let title = new Phaser.BitmapText(
			this.game, 0, 0, "fonts/font_standard", "TURN 'P\nTHE HEAT!!!", 128, "center"
		);
		title.anchor.set(0.5);
		title.y = this._bg.height * 0.052;
		this._bgGroup.addChild(title);

		this._closeButton.visible = false;
		this._closeButton.inputEnabled = false;

		// let image = new Phaser.Image(this.game, 0, 0, "game/background_texture", imgKey);
		// image.anchor.set(0.5);
		// image.scale.set(1.5);
		// image.y = this._bg.height * -0.035;
		// group.addChild(image);

		this.intro().then(() => {
			SoundManager.instance.playFile("sfx/game_start");
		});
	}
}