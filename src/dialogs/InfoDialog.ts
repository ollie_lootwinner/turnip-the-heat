import { BaseDialog } from "./BaseDialog";
import { MiscUtil } from "../../slot-base/src/util/Misc";

export class InfoDialog extends BaseDialog {

	private _leftSide: Phaser.Group;
	private _rightSide: Phaser.Group;

	constructor(game: Phaser.Game, bounds: Phaser.Rectangle) {
		super(game, bounds, "GAME INFO");

		this.onBackdrop.addOnce(() => this.destroy());
		this.onClose.addOnce(() => this.destroy());

		let rtpText = new Phaser.BitmapText(this.game, 0, 0, "fonts/font_standard", "RTP 65%", 26, "center");
		rtpText.anchor.set(0.5);
		rtpText.x = this._bg.width * 0.33;
		rtpText.y = this._bg.height * -0.293;
		this._bgGroup.addChild(rtpText);

		this._leftSide = new Phaser.Group(this.game, this._bgGroup);
		this._leftSide.x = this._bg.width * -0.26;
		this._leftSide.y = this._bg.height * 0.052;

		let leftTitle = new Phaser.BitmapText(
			this.game, 0, 0, "fonts/font_standard", "HOW TO PLAY", 44, "center"
		);
		leftTitle.anchor.set(0.5);
		leftTitle.y = this._bg.height * -0.27;
		this._leftSide.addChild(leftTitle);

		let howToText = new Phaser.BitmapText(
			this.game, 0, 0, "fonts/font_standard",
			`Choose up to 8\nspots to place\nyour bets on.
			\n\nSpots will explode\nuntil 1, 2, or 3\nare left.
			\n\nThese are winning\nspots and will\nreceive a prize!`, 34, "left"
		);
		howToText.anchor.set(0.46, 0.46);
		this._leftSide.addChild(howToText);

		this._rightSide = new Phaser.Group(this.game, this._bgGroup);
		this._rightSide.x = this._bg.width * 0.15;
		this._rightSide.y = this._bg.height * 0.052;

		let middleStrip = new Phaser.Image(this.game, 0, 0, "game/background_texture", "results_panel_middle");
		middleStrip.anchor.set(1, 0.5);
		this._rightSide.addChild(middleStrip);

		let midTitle = new Phaser.BitmapText(
			this.game, 0, 0, "fonts/font_standard", "PAYOUT", 44, "center"
		);
		midTitle.anchor.set(0.5);
		midTitle.x = this._bg.width * -0.127;
		midTitle.y = this._bg.height * -0.27;
		this._rightSide.addChild(midTitle);

		let rightTitle = new Phaser.BitmapText(
			this.game, 0, 0, "fonts/font_standard", "CHANCE", 44, "center"
		);
		rightTitle.anchor.set(0.5);
		rightTitle.x = this._bg.width * 0.124;
		rightTitle.y = this._bg.height * -0.27;
		this._rightSide.addChild(rightTitle);

		let wins: number[] = [2, 3, 4, 5, 10, 15, 25, 50, 100, 1000];
		let chances: number[] = [20, 20, 20, 25, 7.8, 3.0, 2.4, 1.6, 0.19, 0.01];

		for (let i=0; i < 10; i++) {
			let multi = new Phaser.BitmapText(
				this.game, 0, 0, "fonts/font_standard", MiscUtil.formatCurrencyString(wins[i] * 2), 36, "center"
			);
			multi.anchor.set(0.5);
			multi.x = midTitle.x;
			multi.y = this._bg.height * (i * 0.05 - 0.2);
			this._rightSide.addChild(multi);

			let chance = new Phaser.BitmapText(this.game, 0, 0, "fonts/font_standard", `${chances[i]}%`, 36, "center");
			chance.anchor.set(0.5);
			chance.x = rightTitle.x;
			chance.y = multi.y;
			this._rightSide.addChild(chance);
		}

		this.intro();
	}
}