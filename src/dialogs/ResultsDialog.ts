import { BaseDialog } from "./BaseDialog";
import { MiscUtil } from "../../slot-base/src/util/Misc";
import { SoundManager } from "../../slot-base/src/components/SoundManager";

export class ResultsDialog extends BaseDialog {

	constructor(game: Phaser.Game, bounds: Phaser.Rectangle, wins: number[]) {
		super(game, bounds, "FINAL RESULTS");

		this.onClose.addOnce(() => this.destroy());

		let banner = new Phaser.Image(this.game, 0, 0, "game/background_texture", "results_panel_banner");
		banner.anchor.set(0.5);
		banner.y = this._bg.height * -0.41;
		this._bgGroup.addChild(banner);

		this._title.y += this._bg.height * 0.01;

		let middleStrip = new Phaser.Image(this.game, 0, 0, "game/background_texture", "results_panel_middle");
		middleStrip.anchor.set(0.5);
		middleStrip.y = this._bg.height * 0.052;
		this._bgGroup.addChild(middleStrip);

		for (let i=0; i < 3; i++) {
			let group = new Phaser.Group(this.game, this._bgGroup);
			group.x = this._bg.width * (i * 0.26 - 0.26);
			group.y = this._bg.height * 0.052;
			this._bgGroup.addChild(group);

			let title = new Phaser.BitmapText(
				this.game, 0, 0, "fonts/font_standard", `Player ${i + 1}`, 38, "center"
			);
			title.anchor.set(0.5);
			title.y = this._bg.height * -0.245;
			group.addChild(title);

			let imgKey: string = `turnip_${i}_idle_blink_2`;
			let labelText: string = "SORRY,";
			let winAmount: string = "NO WIN";
			let winAmountSize: number = 48;

			if (wins[i] > 599) {
				imgKey = `turnip_${i}_mesmerised_loop_1`;
				labelText = "GREAT WIN!";
				winAmount = MiscUtil.formatCurrencyString( wins[i] / 100 );
				winAmountSize = 64;
			} else if (wins[i] > Number.EPSILON) {
				imgKey = `turnip_${i}_mesmerised_intro_1`;
				labelText = "TOTAL WIN";
				winAmount = MiscUtil.formatCurrencyString( wins[i] / 100 );
				winAmountSize = 64;
			}

			let image = new Phaser.Image(this.game, 0, 0, "game/background_texture", imgKey);
			image.anchor.set(0.5);
			image.scale.set(1.5);
			image.y = this._bg.height * -0.035;
			group.addChild(image);

			let winLabel = new Phaser.BitmapText(
				this.game, 0, 0, "fonts/font_standard", labelText, 32, "center"
			);
			winLabel.anchor.set(0.5);
			winLabel.y = this._bg.height * 0.16;
			group.addChild(winLabel);

			let winText = new Phaser.BitmapText(
				this.game, 0, 0, "fonts/font_standard", winAmount, winAmountSize, "center"
			);
			winText.anchor.set(0.5);
			winText.y = this._bg.height * 0.21;
			group.addChild(winText);
		}

		this.intro().then(() => {
			SoundManager.instance.playFile("sfx/results");
		});
	}
}