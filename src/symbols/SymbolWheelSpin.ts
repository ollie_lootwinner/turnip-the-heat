import { SymbolBase } from "../../slot-base/src/symbols/SymbolBase";
import { SymbolEnum } from "../../slot-base/src/symbols/SymbolEnum";

export class SymbolWheelSpin extends SymbolBase {

	private _isAnticipating: boolean = false;

	constructor(p_game: Phaser.Game, p_symbol: SymbolEnum) {
		super(p_game, p_symbol);
	}

	protected init(p_symbol: SymbolEnum, p_textureKey?: string): void {
		this.createBasicImageAndEffect(p_symbol, "symbol_minigame");

		this._symbolImage.anchor.set(0.5);
		this._symbolImage.scale.set(1.0);
		this._symbolImage.animations.add(
			"active", ["symbol_minigame_2", "symbol_minigame_3", "symbol_minigame_4", "symbol_minigame_5", "symbol_minigame_6",
			"symbol_minigame_7", "symbol_minigame_0", "symbol_minigame_0", "symbol_minigame_0", "symbol_minigame_0"]
		);
		this._symbolImage.animations.add("anticipate", ["symbol_minigame_0", "symbol_minigame_2", "symbol_minigame_3"], 12, true);
		this.add(this._symbolImage);
	}

	public stopAnimating(): void {
		this._isAnticipating = false;
		super.stopAnimating();
	}

	public anticipate(): void {
		if (!this._isAnticipating) {
			this._isAnticipating = true;
			this._symbolImage.animations.play("anticipate");
		}
	}

	public async animate(p_showingAll: boolean): Promise<void> {
		this._isAnticipating = false;
		this._symbolImage.animations.stop("anticipate");

		return new Promise<void>(async (resolve) => {
			this._symbolImage.animations.play("active", 12, false).onComplete.addOnce(() => {
				this._symbolImage.animations.play("active", 12, true).onLoop.addOnce(resolve);
			});
		});
	}
}