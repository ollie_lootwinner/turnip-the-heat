import { SymbolEnum } from "../../slot-base/src/symbols/SymbolEnum";
import { SymbolBase as Base } from "../../slot-base/src/symbols/SymbolBase";
import { LevelUpBar } from "../../slot-base/src/components/LevelUpBar";

export class SymbolBase extends Base {

	protected _symbolImage: Phaser.Image;
	protected _symbolType: number;
	protected _symbolLevel: number;

	protected createBasicImageAndEffect(p_symbol: SymbolEnum, p_textureKey?: string): void {
		let level = 1;
		if (LevelUpBar.instance.symbolCanLevelUp(p_symbol)) {
			level = LevelUpBar.instance.symbolLevel;
		}

		p_textureKey = p_textureKey ? p_textureKey : `symbol_${p_symbol}_level_${level}`;

		this._symbolType = p_symbol;
		this._symbolLevel = level;
		super.createBasicImageAndEffect(p_symbol, p_textureKey);
	}

	public async animate(): Promise<void> {
		this.game.tweens.removeFrom(this);
		this.game.tweens.removeFrom(this._symbolImage);
		this.game.tweens.removeFrom(this._symbolImage.scale);

		// Lowest 3 symbols have flashing animation otherwise scaling animation
		if (this._symbolType < 3) {
			this.fadeTween(this._symbolImage, 500);
		} else {
			if (this._symbolType < 6) {
				this.pulseTween(this._symbolImage, 500);
			}
		}
	}
}