import { Symbol } from "../../slot-base/src/symbols/Symbol";
import { SymbolEnum } from "../../slot-base/src/symbols/SymbolEnum";

import { SymbolCoin } from "./SymbolCoin";
import { SymbolStandard } from "./SymbolStandard";
import { SymbolFreeSpins } from "./SymbolFreeSpins";
import { SymbolWheelSpin } from "./SymbolWheelSpin";

export class SymbolFactory {

	public static createWildSymbol( p_game: Phaser.Game, p_type: SymbolEnum ): Symbol {
		return null;
	}

	public static createScatterSymbol( p_game: Phaser.Game, p_type: SymbolEnum ): Symbol {
		return new SymbolWheelSpin(p_game, p_type);
	}

	public static createCoinSymbol( p_game: Phaser.Game, p_type: SymbolEnum ): Symbol {
		return new SymbolCoin(p_game, p_type);
	}

	public static createFreeSpinsSymbol( p_game: Phaser.Game, p_type: SymbolEnum ): Symbol {
		return new SymbolFreeSpins(p_game, p_type);
	}

	public static createStandardSymbol( p_game: Phaser.Game, p_type: SymbolEnum ): Symbol {
		return new SymbolStandard(p_game, p_type);
	}
}