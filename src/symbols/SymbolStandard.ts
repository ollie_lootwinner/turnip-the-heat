import { SymbolBase } from "./SymbolBase";

export class SymbolStandard extends SymbolBase {

	public set blurred(p_blurred: boolean) {
		this._symbolImage.frameName = p_blurred ? `${this.frameKey}_1` : `${this.frameKey}_0`;
	}

	public set highlighted(p_highlighted: boolean) {
		// this._symbolImage.frameName = p_highlighted ? `${this.frameKey}_2` : `${this.frameKey}_0`; For Reference if we bring Highlighting back.

		if (!p_highlighted) {
			this.stopAnimating();
		}
	}
}