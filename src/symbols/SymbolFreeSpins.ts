import { SymbolEnum } from "../../slot-base/src/symbols/SymbolEnum";
import { SymbolBase } from "../../slot-base/src/symbols/SymbolBase";

export class SymbolFreeSpins extends SymbolBase {

	private _isAnticipating: boolean = false;

	constructor(p_game: Phaser.Game, p_symbol: SymbolEnum) {
		super(p_game, p_symbol);
	}

	protected init(p_symbol: SymbolEnum, p_textureKey?: string): void {
		this.createBasicImageAndEffect(p_symbol, "symbol_freespins");

		this._symbolImage.anchor.set(0.5);
		this._symbolImage.scale.set(1.0);
		this._symbolImage.animations.add(
			"active", ["symbol_freespins_2", "symbol_freespins_3", "symbol_freespins_4", "symbol_freespins_5",
			"symbol_freespins_6", "symbol_freespins_7", "symbol_freespins_8", "symbol_freespins_9"], 10
		);
		this._symbolImage.animations.add(
			"activeLoop", ["symbol_freespins_10", "symbol_freespins_11", "symbol_freespins_12", "symbol_freespins_13", "symbol_freespins_14", "symbol_freespins_15",
			"symbol_freespins_10", "symbol_freespins_10", "symbol_freespins_10", "symbol_freespins_10", "symbol_freespins_10", "symbol_freespins_10"], 10
		);
		this.add(this._symbolImage);
	}

	public stopAnimating(): void {
		this._isAnticipating = false;
		super.stopAnimating();
	}

	public anticipate(): void {
		if (!this._isAnticipating) {
			this._isAnticipating = true;
			this._symbolImage.animations.play("active", 10, true);
		}
	}

	public async animate(p_showingAll: boolean): Promise<void> {
		this._isAnticipating = false;
		this._symbolImage.animations.stop("active");

		return new Promise<void>(async (resolve, reject) => {
			this._symbolImage.animations.play("active", 10, false).onComplete.addOnce(() => {
				this._symbolImage.animations.play("activeLoop", 10, true).onLoop.addOnce(resolve);
			});
		});
	}
}