import { SymbolCoin as BaseCoin } from "../../slot-base/src/symbols/SymbolCoin";
import { SymbolEnum } from "../../slot-base/src/symbols/SymbolEnum";

export class SymbolCoin extends BaseCoin {

	protected init(p_symbol: SymbolEnum, p_textureKey?: string): void {
		this.flatFrame = "symbol_coin_0";
		this.spinFrames = [
			"symbol_coin_0", "symbol_coin_1", "symbol_coin_2", "symbol_coin_3", "symbol_coin_4", "symbol_coin_5"
		];
		super.init(p_symbol, p_textureKey);
	}
}