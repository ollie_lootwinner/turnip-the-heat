import { SideBarGroup } from "./SideBarGroup";
import { Reels } from "../../slot-base/src/reels/Reels";
import { Reel } from "../../slot-base/src/reels/Reel";
import { SymbolEnum } from "../../slot-base/src/symbols/SymbolEnum";
import { BackgroundReels } from "../../slot-base/src/reels/BackgroundReels";
import { Symbol } from "../../slot-base/src/symbols/Symbol";
import { ReelConfig } from "../../slot-base/src/reels/ReelConfig";
import { SlotConfig, AnticipateMode, LevelUpMode, ScatterTeaseRate } from "../../slot-base/src/slotMath/SlotConfig";
import { LevelUpBar } from "../../slot-base/src/components/LevelUpBar";

import { IGameDisplay } from "../../slot-base/src/components/IGameDisplay";
import { DividerBarAnimations } from "./DividerBarAnimations";
import { MiscUtil } from "../../slot-base/src/util/Misc";
import { RevealChests } from "./RevealChests";
import { SlotResult } from "../../slot-base/src/slotMath/SlotResult";
import { GameResult } from "../../slot-base/src/states/AbstractGameState";
import { GameConfig } from "../config/GameConfig";
import { TableName } from "../../slot-base/src/slotMath/paytables/Paytable";

export class GameDisplay extends IGameDisplay {

	private _result: SlotResult;
	protected _revealer: RevealChests;

	private _displayGroup: Phaser.Group;
	public get displayGroup(): Phaser.Group { return this._displayGroup; }

	private _activeAnticipationSymbols: Symbol[] = [];
	private _dividerWidth: number;

	// Background and Overlays
	private _bgNormal: Phaser.Image;

	// Borders
	private _topBarHeight: number;
	private _sideLeft: SideBarGroup;
	private _sideRight: SideBarGroup;

	public async buildStage(p_game: Phaser.Game, area: Phaser.Group, bounds: Phaser.Rectangle): Promise<void> {

		await super.buildStage(p_game, area, bounds);

		// Setup the Level up Gem Game mode
		SlotConfig.setLevelUpMode(LevelUpMode.LEVEL_UP_GEMS_ACTIVE);

		this._game = p_game;
		this._topBarHeight = bounds.height * (100 / 1080);

		let reelOffset: number = bounds.width * (290 / 1920);
		let columnWidth: number = bounds.width * (420 / 1920);
		this._dividerWidth = bounds.width * (40 / 1920);
		let columnHeight: number = bounds.height * (780 / 1080);

		////////////////////////////////////////////////
		// Background
		////////////////////////////////////////////////
		this._bgNormal = new Phaser.Image(p_game, 0, 0, "bg/base");
		this._bgNormal.height = bounds.height;
		this._bgNormal.scale.x = this._bgNormal.scale.y;
		area.add(this._bgNormal);

		this._displayGroup = new Phaser.Group(this._game, area);

		////////////////////////////////////////////////
		// Statues and Jaw
		////////////////////////////////////////////////
		this._sideLeft = new SideBarGroup(p_game, bounds, false);
		this._sideLeft.position.x = reelOffset;
		this._sideLeft.position.y = -this._topBarHeight / 2;

		this._sideRight = new SideBarGroup(p_game, bounds, true);
		this._sideRight.position.x = bounds.right - reelOffset;
		this._sideRight.position.y = -this._topBarHeight / 2;

		////////////////////////////////////////////////
		// Dividers and columns
		////////////////////////////////////////////////
		let _dividers: Phaser.Image[] = [];
		let _columns: Phaser.Image[] = [];
		let _stars: Phaser.Image[] = [];
		let _iterator: number;

		DividerBarAnimations.init(p_game);

		for (_iterator = 0 ; _iterator < 3 ; _iterator++) {

			// Dividers start one column in
			if (_iterator > 0) {
				let reelDivider: Phaser.Image = new Phaser.Image(p_game, 0, 0, "game/background_texture", "reel_dividerbackplate");
				reelDivider.width = this._dividerWidth;
				reelDivider.height = columnHeight;

				// Stars on the column dividers will go here
				let starsOnEachDivider = 9;
				// Find gap between each star (and top and bottom)
				let gapBetweenStars = columnHeight / starsOnEachDivider + 1;
				let currentGap: number = 40;
				// There are 9 stars on each reel
				for(let yPos = 0; yPos < starsOnEachDivider; yPos++) {
					currentGap += gapBetweenStars;
					let newStar = new Phaser.Image(p_game, 0, 0, "game/background_texture", "reel_divider_star");
					newStar.anchor.set(0.5);
					let newStarXPos: number = reelOffset + (columnWidth * (_iterator)) + (this._dividerWidth * (_iterator - 1));
					newStar.position.set(newStarXPos + 20, currentGap + 5);
					_stars.push(newStar);
					DividerBarAnimations.addStarToColumn(newStar, _iterator);
				}

				let reelOverlay: Phaser.Image = new Phaser.Image(p_game, 0, 0, "game/background_texture", "reel_dividerbase");
				reelOverlay.width = this._dividerWidth;
				reelOverlay.height = columnHeight;

				let newDividerPosition: number = reelOffset + (columnWidth * (_iterator)) + (this._dividerWidth * (_iterator - 1));
				reelDivider.position.set(newDividerPosition, this._topBarHeight);
				reelOverlay.position.set(newDividerPosition, this._topBarHeight);
				_dividers.push(reelDivider, reelOverlay);
			}

			let reelBGNormal: Phaser.Image = new Phaser.Image(p_game, 0, 0, "game/background_texture", "reel_standard_pixel");
			let reelBGFreeSpin: Phaser.Image = new Phaser.Image(p_game, 0, 0, "game/background_texture", "reel_freespins");
			let reelBGAnticipate: Phaser.Image = new Phaser.Image(p_game, 0, 0, "game/background_texture", "reel_anticipation");
			let reelBGWin: Phaser.Image = new Phaser.Image(p_game, 0, 0, "game/background_texture", "reel_winning");
			// let reelBGWinGlow: Phaser.Image = new Phaser.Image(p_game, 0, 0, "game/background_texture", "reel_bigwinhighlighteffect");

			// Needed for slight displacement of reels
			let pixelOffset = bounds.width * (-1 / 1920);
			reelBGNormal.position.set((reelOffset + (columnWidth * _iterator + 1) + (this._dividerWidth * _iterator)) + pixelOffset, this._topBarHeight);
			// reelBGNormal.alpha = 0.5;
			reelBGNormal.width = ReelConfig.symbolWidth;
			reelBGNormal.height = ReelConfig.symbolHeight * 3;
			reelBGFreeSpin.position = reelBGNormal.position;
			reelBGFreeSpin.alpha = 0;
			reelBGAnticipate.position = reelBGNormal.position;
			reelBGAnticipate.alpha = 0;
			reelBGWin.position = reelBGNormal.position;
			reelBGWin.alpha = 0;
			// reelBGWinGlow.position = reelBGNormal.position;
			// reelBGWinGlow.alpha = 0;
			_columns.push(reelBGNormal);
			BackgroundReels._freeSpinReels.push(reelBGFreeSpin);
			BackgroundReels._anticipateReels.push(reelBGAnticipate);
			BackgroundReels._winReels.push(reelBGWin);
			// BackgroundReels._winGlow.push(reelBGWinGlow);
		}

		////////////////////////////////////////////////
		// Reels
		////////////////////////////////////////////////
		let reelW: number = bounds.width * (1300 / 1920);
		this._reels = new Reels(p_game, reelW, columnHeight, 0, this._dividerWidth);
		this._reels.init();
		this._reels.centerX = bounds.centerX;
		this._reels.y = this._topBarHeight;

		let reelMask = this._game.add.graphics(reelOffset, this._topBarHeight);
		reelMask.beginFill(0x000000, 0);
		reelMask.drawRect(0, 0, reelW + 4 * this._dividerWidth, columnHeight);
		this._reels.mask = reelMask;

		////////////////////////////////////////////////
		// reel signals
		////////////////////////////////////////////////
		let currentReels: Reel[] = this.innerReels;
		for (let setReel of currentReels) {
			setReel._toAnticipate.add( () => { this.decideAnticipation(); } );
		}

		////////////////////////////////////////////////
		// Paylines
		////////////////////////////////////////////////
		this._awardedFreeSpins = new Phaser.BitmapText(p_game, 0, 0, "fonts/font_win", "+@@ free spins won", 90, "center");
		this._awardedFreeSpins.scale.set(0, 0);
		this._awardedFreeSpins.anchor.set(0.5, 0.5);
		this._awardedFreeSpins.position.set(bounds.width / 2, bounds.height / 2);
		this._awardedFreeSpins.visible = false;

		////////////////////////////////////////////////
		// Fading Background
		////////////////////////////////////////////////
		this._backgroundFade = new Phaser.Graphics(p_game, 0, 0);
		this._backgroundFade.beginFill(0x000000, 0.8);
		this._backgroundFade.drawRect(-1, -1, bounds.width + 2, bounds.height + 2);
		this._backgroundFade.endFill();
		this._backgroundFade.alpha = 0;

		////////////////////////////////////////////////
		// Revealer
		////////////////////////////////////////////////
		this._revealer = new RevealChests(this.game);

		////////////////////////////////////////////////
		// Addng children
		////////////////////////////////////////////////
		this._displayGroup.add(this._sideLeft);
		this._displayGroup.add(this._sideRight);

		// Adds the normal columns
		for (let newColumn of _columns) {
			this._displayGroup.add(newColumn);
		}

		// Overlayed free spin columns
		for (let newFreeSpinColumn of BackgroundReels._freeSpinReels) {
			this._displayGroup.add(newFreeSpinColumn);
		}

		// Overlayed anticipate spin columns
		for (let newAnticipateColumn of BackgroundReels._anticipateReels) {
			this._displayGroup.add(newAnticipateColumn);
		}

		// Overlayed win + win glow
		for (let newWinColumn of BackgroundReels._winReels) {
			this._displayGroup.add(newWinColumn);
		}
		// for (let newWinColumn of BackgroundReels._winGlow) {
		// 	this._displayGroup.add(newWinColumn);
		// }

		// Column dividers
		for (let newDivider of _dividers) {
			this._displayGroup.add(newDivider);
		}

		// Add Level Up Bar
		if (SlotConfig.levelUpActive === LevelUpMode.LEVEL_UP_GEMS_ACTIVE) {

			// Create the bar
			LevelUpBar.instance.init(this._game, this._displayGroup, bounds);
			LevelUpBar.instance.onLevelUp.add((newLevel: number) => {
				if (newLevel === 2) {
					GameConfig.PAYTABLE_NAME = TableName.GEM_BOOSTER_LEVEL_TWO;
					GameConfig.PAYTABLE_NAME_FREESPINS = TableName.GEM_BOOSTER_LEVEL_TWO;
					SlotConfig.scatterTeaseRate = ScatterTeaseRate.MEDIUM;
				} else {
					GameConfig.PAYTABLE_NAME = TableName.GEM_BOOSTER_LEVEL_THREE;
					GameConfig.PAYTABLE_NAME_FREESPINS = TableName.GEM_BOOSTER_LEVEL_THREE;
					SlotConfig.scatterTeaseRate = ScatterTeaseRate.HIGH;
				}
			});
		}

		// Stars on columns
		for (let newStar of _stars) {
			this._displayGroup.add(newStar);
		}

		this._displayGroup.add(this._revealer);
		this._displayGroup.add(this._reels);
		this._displayGroup.add(this._lineWinAmountText);
		this._displayGroup.add(this._backgroundFade);
		this._displayGroup.add(this._awardedFreeSpins);

		(this._reels as Reels).spinAnticipation.add(() => {
			BackgroundReels.setAnticipate(this._game, 3, true);
		});
		BackgroundReels.init(this._game);
	}

	public async startSpin(): Promise<void> {
		await this._revealer.hide( (this._reels as Reels).reels );
	}

	public async stopSpin(p_result: GameResult): Promise<void> {
		this._result = p_result as SlotResult;
		let spinComplete: Promise<void> = super.stopSpin(this._result);

		let getSymbolPos: (a: number, b: number) => Phaser.Point = (a: number, b: number) => {
			return (this._reels as Reels).getSymbolPos(a, b);
		};

		await MiscUtil.timeoutPromise(this.game, 300);
		await this._revealer.reveal( (this._reels as Reels).reels, this._result.reelsToAnticipate, getSymbolPos );
		await spinComplete;
	}

	// Free spins, ruby or both
	private decideAnticipation() {
		if (SlotConfig.anticipationMode === AnticipateMode.HIGHEST_SYMBOL
			|| SlotConfig.anticipationMode === AnticipateMode.BOTH) {
			this.anticipateSymbol( (ReelConfig.NUM_BASIC_SYMBOLS - 1) as SymbolEnum );
		}
		if (SlotConfig.anticipationMode === AnticipateMode.FREESPINS
			|| SlotConfig.anticipationMode === AnticipateMode.BOTH) {
			this.anticipateSymbol(SymbolEnum.FREESPINS);
		}
	}

	private anticipateSymbol(p_type: SymbolEnum) {
		for (let i = 0; i < this.innerReels.length; i++) {
			if (!this.innerReels[i].isCurrentlySpinning) {
				for (let j = 0; j < this.innerReels[i].symbols.length; j++) {
					let sym = this.innerReels[i].symbols[j];
					if (sym.type === p_type && (typeof (sym as any).anticipate === "function")) {
						(sym as any).anticipate();
						this._activeAnticipationSymbols.push(sym);
					}
				}
			} else {
				break;
			}
		}
	}

	public stopSymbolAnticipation() {
		for (let i=0; i < this._activeAnticipationSymbols.length; i++) {
			this._activeAnticipationSymbols[i].stopAnimating();
		}
		this._activeAnticipationSymbols = [];
	}

	public async onFreespinsTriggered() {
		BackgroundReels.setWinReel(this._game, true);
		await super.onFreespinsTriggered();
	}

	public enableFreeSpins() {
		BackgroundReels.setFreeSpin(this._game, true);
	}

	public disableFreeSpins() {
		BackgroundReels.setFreeSpin(this._game, false);
	}

	public destroy() {
		BackgroundReels.clear();
		super.destroy();
	}
}