export class GameStatePanel extends Phaser.Group {

	private _bg: Phaser.Image;
	private _stateText: Phaser.BitmapText;

	public init(state: string) {
		this._bg = new Phaser.Image(this.game, 0, 0, "game/background_texture", "gamestate_panel");
		this._bg.anchor.set(1, 0.5);
		this.addChild(this._bg);

		this._stateText = new Phaser.BitmapText(this.game, 0, 0, "fonts/font_standard", state, 36, "center");
		this._stateText.anchor.set(0.5, 0.5);
		this._stateText.x = this._bg.width * -0.5;
		this._stateText.y = this._bg.height * 0.03;
		this.addChild(this._stateText);
	}

	public updateState(state: string) {
		this._stateText.text = state;
	}
}