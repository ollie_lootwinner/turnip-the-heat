import { MiscUtil } from "../../slot-base/src/util/Misc";
import { ReelConfig } from "../../slot-base/src/reels/ReelConfig";
import { Reel } from "../../slot-base/src/reels/Reel";
import { Symbol } from "../../slot-base/src/symbols/Symbol";
import { LevelUpBar } from "../../slot-base/src/components/LevelUpBar";
import { SoundManager } from "../../slot-base/src/components/SoundManager";

export class RevealChests extends Phaser.Group {

	public async hide(reels: Reel[]): Promise<void> {
		await MiscUtil.timeoutPromise(this.game, 200);
		SoundManager.instance.playFile("sfx/symbols_fade");

		for (let y=0; y < ReelConfig.NUM_SYMBOLS_ON_FACE; y++) {
			for (let x=0; x < ReelConfig.NUM_REELS; x++) {
				let sym: Symbol = reels[x].symbols[ReelConfig.NUM_SYMBOLS_ABOVE_GAME_AREA + y];
				this.game.add.tween(sym.scale)
				.to({ x: 0.7, y: 0.7 }, 400, Phaser.Easing.Back.In, true, 200 * y);
				this.game.add.tween(sym)
				.to({ alpha: 0 }, 400, Phaser.Easing.Linear.None, true, 200 * y);
			}
		}

		await MiscUtil.timeoutPromise(this.game, 1000);

		for (let x=0; x < ReelConfig.NUM_REELS; x++) {
			reels[x].alpha = 0;
		}
	}

	public async reveal(reels: Reel[], anticipationReels: number[], getSymbolPos: (a: number, b: number) => Phaser.Point): Promise<void> {
		for (let x=0; x < ReelConfig.NUM_REELS; x++) {
			reels[x].alpha = 1;
			for (let y=0; y < ReelConfig.NUM_SYMBOLS_ON_FACE; y++) {
				let sym: Symbol = reels[x].symbols[ReelConfig.NUM_SYMBOLS_ABOVE_GAME_AREA + y];
				sym.alpha = 0;
				sym.scale.set(0.4, 0.4);
			}
		}

		let revealPromises: Promise<void>[] = [];

		for (let y = ReelConfig.NUM_SYMBOLS_ON_FACE - 1; y >= 0; y--) {
			for (let x=0; x < ReelConfig.NUM_REELS; x++) {

				let chest: Phaser.Image = new Phaser.Image(this.game, 0, 0, "game/symbols_texture", "chest_0");
				chest.anchor.set(0.5);
				chest.scale.x = 0.9;
				chest.scale.y = 1.1;
				chest.animations.add("open", ["chest_0", "chest_1", "chest_2", "chest_3", "chest_4", "chest_5", "chest_6"]);
				this.add(chest);

				let pos: Phaser.Point = getSymbolPos(x, y);
				chest.position.set(pos.x, pos.y);

				this.game.add.tween(chest)
				.from(
					{ y: pos.y - this.game.height * 0.85 }, 600,
					(k) => {
						let s = 1.20158;
						return --k * k * ((s + 1) * k + s) + 1;
					}, true, x * 150
				)
				.onComplete.addOnce( async () => {

					await MiscUtil.timeoutPromise(this.game, 200);

					revealPromises.push(
						new Promise<void>( async (resolve) => {
							if (anticipationReels.includes(x)) {
								await this.anticipateSymbol(chest);
							}
							await this.revealSymbol(chest, reels[x].symbols[ReelConfig.NUM_SYMBOLS_ABOVE_GAME_AREA + y], x, y);

							if (anticipationReels.includes(x)) {
								await MiscUtil.timeoutPromise(this.game, 400);
							}
							resolve();
						})
					);
				});

				this.game.add.tween(chest.scale)
				.to({ x: 1.1, y: 0.9 }, 150, Phaser.Easing.Linear.None, true, x * 150 + 250)
				.chain(
					this.game.add.tween(chest.scale)
					.to({ x: 1.0, y: 1.0 }, 150, Phaser.Easing.Linear.None, true)
				)
				.onStart.addOnce(() => {
					SoundManager.instance.playFile("sfx/chest_falls");

					for (let i=0; i < 2; i++) {
						let smoke: Phaser.Image = new Phaser.Image(this.game, 0, 0, "game/symbols_texture", "chest_smoke");
						smoke.anchor.set(0.5);
						smoke.y = pos.y + chest.height * 0.4;
						this.add(smoke);

						let posX: number = chest.x - chest.width * 0.1;
						let destX: number = chest.x - chest.width * 0.4;
						if (i === 1) {
							smoke.scale.x = -1;
							posX = chest.x + chest.width * 0.1;
							destX = chest.x + chest.width * 0.4;
						}

						smoke.x = posX;
						this.game.add.tween(smoke.scale)
						.to({ x: 0.5, y: 0.5 }, 200, Phaser.Easing.Linear.None, true);
						this.game.add.tween(smoke)
						.to({ x: destX, alpha: 0 }, 250, Phaser.Easing.Linear.None, true)
						.onComplete.addOnce(() => {
							smoke.destroy();
						});
					}
				});
			}

			await MiscUtil.timeoutPromise(this.game, 400);
		}
		LevelUpBar.instance.updateLevelUpBarMask(1);
		await Promise.all(revealPromises);
	}

	private async anticipateSymbol(chest: Phaser.Image): Promise<void> {
		let reps: number = Math.floor( Math.random() * 3) + 3;
		for (let i=0; i < reps; i++) {
			await MiscUtil.tweenCompletePromise(
				this.game.add.tween(chest).to({ angle: (i % 2 === 0) ? -5 : 5 }, 200, Phaser.Easing.Linear.None, true, 0, 0, true)
			);
		}
	}

	private async revealSymbol(chest: Phaser.Image, sym: Symbol, row: number, col: number): Promise<void> {
		chest.animations.play("open", 10, false);
		SoundManager.instance.playFile("sfx/chest_opens");

		this.game.add.tween(chest)
		.to({ alpha: 0 }, 250, Phaser.Easing.Linear.None, true, 700)
		.onComplete.addOnce(() => {
			chest.destroy();
		});

		return new Promise<void>((resolve) => {
			let originY: number = sym.y;

			MiscUtil.timeoutPromise(this.game, 150).then(() => {
				LevelUpBar.instance.particleBurst(row, col);
			});

			this.game.add.tween(sym)
			.to({ alpha: 1, y: sym.y - chest.height * 0.35 }, 350, Phaser.Easing.Linear.None, true, 150)
			.onComplete.addOnce(() => {
				this.game.add.tween(sym)
				.to({ y: sym.y + chest.height * 0.4 }, 250, Phaser.Easing.Linear.None, true)
				.onComplete.addOnce(() => {
					this.game.add.tween(sym)
					.to({ y: originY }, 150, Phaser.Easing.Linear.None, true)
					.onComplete.addOnce(() => {
						resolve();
					});
				});
			});

			this.game.add.tween(sym.scale)
			.to({ x: 1.0, y: 1.0 }, 600, Phaser.Easing.Linear.None, true, 150);
		});
	}
}