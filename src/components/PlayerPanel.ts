import { Bet } from "./BetUI";

export class Player {
	constructor(
		public id: number,
		public bets: Bet[]
	) {}
}

export class PlayerPanel extends Phaser.Group {

	private _bg: Phaser.Image;
	private _players: Player[];
	private _playerDisplays: Phaser.Image[];

	public init() {
		this._players = [];
		this._playerDisplays = [];

		this._bg = new Phaser.Image(this.game, 0, 0, "game/background_texture", "current_players_panel");
		this._bg.anchor.set(1, 0.5);
		this.addChild(this._bg);

		for (let i=0; i < 3; i++) {
			this._players.push( new Player(i, []) );

			this._playerDisplays[i] = new Phaser.Image(this.game, 0, 0, "game/background_texture", `tag_player_${i}`);
			this._playerDisplays[i].anchor.set(1, 0.5);
			this._playerDisplays[i].y = this._bg.height * (i * 0.22 - 0.215);
			this.addChild(this._playerDisplays[i]);

			this.updatePlayerTag(this._players[i]);
		}
	}

	public updatePlayerTag(player: Player) {
		if (this._playerDisplays[player.id].children[0]) {
			(this._playerDisplays[player.id].children[0] as Phaser.BitmapText).destroy();
		}

		let displayText: Phaser.BitmapText = new Phaser.BitmapText(
			this.game, 0, 0, "fonts/font_standard", `${player.bets.length} BET`, 52
		);
		if (player.bets.length === 0 || player.bets.length > 1) {
			displayText.text += "S";
		}
		displayText.anchor.set(0, 0.5);
		displayText.x = this._playerDisplays[player.id].width * -0.6;
		displayText.y = this._playerDisplays[player.id].height * 0.1;
		this._playerDisplays[player.id].addChild(displayText);
	}
}