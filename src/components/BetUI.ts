import { DisablingButton } from "../../slot-base/src/components/DisablingButton";
import { SignalDispatcher } from "../../slot-base/src/components/SignalDispatcher";
import { SoundManager } from "../../slot-base/src/components/SoundManager";
import { MiscUtil } from "../../slot-base/src/util/Misc";
import { MathUtil } from "../../slot-base/src/util/Math";
import { BettingConfig } from "../../slot-base/src/reels/BettingConfig";
import { PaylinesConfig } from "../../slot-base/src/reels/PaylinesConfig";
import { BalanceManager } from "../../slot-base/src/BalanceManager";
import { GenericTextInfoDialog, GenericDlgTitle } from "../../slot-base/src/dialogs/GenericTextInfoDialog";
import { InfoDialog } from "../../slot-base/src/dialogs/InfoDialog";
import { InfoDialog as HelpDialog } from "../dialogs/InfoDialog";
import { Panel } from "./Panel";
import { GameStatePanel } from "./GameStatePanel";
import { PlayerPanel, Player } from "./PlayerPanel";

export class Bet {
	constructor(
		public amount: number,
		public location: number
	) {}
}

export class BetUI extends Phaser.Group {

	protected _bounds: Phaser.Rectangle;

	protected _gamestate: GameStatePanel;
	public get gamestate(): GameStatePanel { return this._gamestate; }
	protected _players: PlayerPanel;
	public get players(): PlayerPanel { return this._players; }

	protected _spinButton: DisablingButton;
	protected _infoButton: DisablingButton;

	protected _titleBadge: Phaser.Image;
	protected _betCostText: Phaser.BitmapText;
	protected _maxBetsText: Phaser.BitmapText;

	protected _totalBetAmount: number = 0;
	protected _bets: Bet[];

	public init(p_bounds: Phaser.Rectangle) {
		this._bounds = p_bounds;
		this.createBottomUI();
	}

	protected createBottomUI() {

		////////////////////////////////////////////////
		// Right Hand Panels
		////////////////////////////////////////////////

		this._gamestate = new GameStatePanel(this.game);
		this._gamestate.init("PLACE\nYOUR\nBETS");
		this._gamestate.right = this._bounds.width ;
		this._gamestate.top = 0;

		this._players = new PlayerPanel(this.game);
		this._players.init();
		this._players.right = this._bounds.width;
		this._players.y = this._bounds.height * 0.49;

		////////////////////////////////////////////////
		// Bottom Text
		////////////////////////////////////////////////

		let yPos: number = this._bounds.height * 0.957;

		this._titleBadge = new Phaser.Image(this.game, 0, 0, "game/background_texture", "game_title");
		this._titleBadge.anchor.set(0.5);
		this._titleBadge.scale.set(0.3);
		this._titleBadge.x = this.game.width * 0.43;
		this._titleBadge.y = this._bounds.height * 0.91;

		this._maxBetsText = new Phaser.BitmapText(
			this.game, 0, 0, "fonts/font_standard", "MAXIMUM POTS: 8", 44, "center",
		);
		this._maxBetsText.anchor.set(0.5, 0.5);
		this._maxBetsText.x = this._bounds.width * 0.22; // 0.27;
		this._maxBetsText.y = yPos;

		this._betCostText = new Phaser.BitmapText(
			this.game, 0, 0, "fonts/font_standard",
			`COST PER BET: ${MiscUtil.formatCurrencyString(BettingConfig.BET_CHANGE_AMOUNT / 100)}`, 44, "center",
		);
		this._betCostText.anchor.set(0.5, 0.5);
		this._betCostText.x = this._bounds.width * 0.652; // 0.57;
		this._betCostText.y = yPos;

		////////////////////////////////////////////////
		// Main Buttons
		////////////////////////////////////////////////

		this._spinButton = new DisablingButton(
			this.game, 0, 0, "game/background_texture", "play_1", "play_0", "play_2", "play_0", "play_3"
		);
		this._spinButton.anchor.set(1, 1);
		this._spinButton.scale.set(0.8);
		this._spinButton.position.set(this._bounds.width * 0.992, this._bounds.height * 0.99);
		this._spinButton.onInputUp.add(() => {
			this.dispatchSpinSignal();
		});

		this._infoButton = new DisablingButton(
			this.game, 10, this._bounds.height - 10, "game/background_texture", "howTo_1", "howTo_0", "howTo_2", "howTo_0", "howTo_0"
		);
		this._infoButton.anchor.set(0, 1);
		this._infoButton.onInputDown.add(() => this.onInfoClicked(), this);

		this.addChild(this._titleBadge);

		this.add(this._players);
		this.add(this._gamestate);

		this.add(this._spinButton);
		this.add(this._infoButton);

		this.add(this._maxBetsText);
		this.add(this._betCostText);

		this.forceBalanceUpdate();
		this.onGameOver();
	}

	public set tutorialButtonDisabled(p_value: boolean) {
		this._infoButton.disabled = p_value;
	}

	public forceBalanceUpdate(): void {
		this.setMyWagersBalance(BalanceManager.currentManager.availableGameplayBalance);
	}

	protected setMyWagersBalance(newBalance: number): void {
		let newText: string = MiscUtil.formatBalanceString(newBalance);
	}

	public onInfoClicked() {
		let info: HelpDialog = new HelpDialog(this.game, this._bounds);
		this.addChild(info);
	}

	public checkEnoughBalance(): Promise<void> {
		return new Promise<void>((resolve) => {
			if (!this.canAfford(BalanceManager.currentManager.availableGameplayBalance, this._totalBetAmount)) {
				let dlg: InfoDialog = GenericTextInfoDialog.create(
					this.game, this._bounds.width, this._bounds.height, GenericDlgTitle.out_of_points, new Phaser.Point(0.8, 0.5)
				);
				dlg.onConfirmed.addOnce(() => {
					dlg.destroy();
					resolve();
				});
				this.add(dlg);
			} else {
				resolve();
			}
		});
	}

	protected canAfford(p_balance: number, p_betAmount: number): boolean {
		return Phaser.Math.fuzzyGreaterThan(p_balance, p_betAmount, MathUtil.EPSILON);
	}

	protected dispatchSpinSignal() {
		if (!this.uiIsLocked) {
			SignalDispatcher.spinButtonClickedSignal.dispatch(this._totalBetAmount, this._bets);
		}
	}

	public onPanelTapped(panel: Panel, pointer: Phaser.Pointer) {

		let wasSelected: boolean = panel.selected;
		if (!wasSelected && this._bets.length > 7) {
			SoundManager.instance.playFile("sfx/bet_invalid", 0.75);
			return;
		}

		if (wasSelected) {
			let index: number = -1;
			for (let i=0; i < this._bets.length; i++) {
				if (this._bets[i].location === panel.value) {
					index = i;
					break;
				}
			}

			// Check for left click - add money to a current bet
			if (false && pointer.isMouse && pointer.leftButton.timeUp > new Date().valueOf() - 200) {
				if (index >= 0) {
					if ((this._bets[index].amount + Number.EPSILON) > BettingConfig.MAX_BET_AMOUNT) {
						// TODO play incorrect sfx
					} else {
						this._bets[index].amount += BettingConfig.BET_CHANGE_AMOUNT;
					}
				}
			} else { // Right/Middle click - bet removal

				panel.setSelected(false, 0);
				if (index >= 0) {
					this._bets.splice(index, 1);
				}
			}
		} else {// if (!pointer.isMouse || pointer.leftButton.timeUp > new Date().valueOf() - 200) { // Only add bet on left click
			panel.setSelected(true, 0);
			let newBet: Bet = new Bet(BettingConfig.BET_CHANGE_AMOUNT, panel.value);
			this._bets.push(newBet);
		}

		this._totalBetAmount = 0;
		for (let i=0; i < this._bets.length; i++) {
			this._totalBetAmount += this._bets[i].amount;
		}
		this._totalBetAmount = Math.round(this._totalBetAmount);

		this.onBetTotalChanged();
	}

	protected decrementBetAmount(): void {
		this.modifyBetAmount(0 - BettingConfig.BET_CHANGE_AMOUNT);
	}

	protected incrementBetAmount(): void {
		this.modifyBetAmount(BettingConfig.BET_CHANGE_AMOUNT);
	}

	protected modifyBetAmount(p_changeAmount: number): void {
		this._totalBetAmount = Math.round(this._totalBetAmount + p_changeAmount);
		this.onBetTotalChanged();
	}

	protected onBetTotalChanged(): void {

		// Update the text total
		this._players.updatePlayerTag( new Player(0, this._bets) );

		// Refresh all UI button states
		this.refreshUI();
	}

	public lockUI(): void {
		this._spinButton.disabled = true;
	}

	public get uiIsLocked(): boolean {
		return this._spinButton.disabled; // Only disabled when the UI is locked
	}

	public async refreshUI(shouldAutoPlay?: boolean) {
		this._spinButton.disabled = this._bets.length === 0 || this._bets.length > PaylinesConfig.lines.length;
	}

	public async onGameOver() {
		this._bets = [];
		this._totalBetAmount = 0;
		this._gamestate.updateState("PLACE\nYOUR\nBETS");
		this._players.updatePlayerTag( new Player(1, []) );
		this._players.updatePlayerTag( new Player(2, []) );
		this.onBetTotalChanged();
	}

	public destroy(): void {
		this.game.input.onTap.removeAll();
		super.destroy();
	}
}