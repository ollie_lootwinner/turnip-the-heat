/// <reference path="../../slot-base/typings/phaser.comments.d.ts" />

export class BackgroundParticles extends Phaser.Group {
	private _particlePool: Phaser.Image[] = [];
	private _NUM_PARTICLES: number = 0;
	private _MAX_TIME: number = 15000;
	private _MIN_TIME: number = 7500;

	constructor(p_game: Phaser.Game, p_width) {
		super(p_game);
		this.createParticles(p_game, p_width);
	}

	private createParticles(p_game: Phaser.Game, p_width: number) {
		let toChoose = ["background_particle1", "background_particle2"];

		for (let i=0; i < this._NUM_PARTICLES; i++) {
			let chosen = toChoose[Math.floor(Math.random() * toChoose.length)];
			let xPos = Math.random() * p_width - (p_width * 0.3);
			let newPart = new Phaser.Image(p_game, xPos , -p_game.height * 0.5, "game/background_texture", chosen);
			newPart.rotation = Math.floor(Math.random() * 360);
			newPart.scale.set(0.5);
			newPart.alpha = 1;

			let y_amount = p_game.height * Math.random();
			let duration = Math.max(Math.random() * this._MAX_TIME, this._MIN_TIME);
			let delay = Math.random() * this._MIN_TIME;

			// p_game.add.tween(newPart).to({ y: y_amount, alpha: 0 }, duration, Phaser.Easing.Linear.None, true, delay, -1);
			p_game.add.tween(newPart).to({ y: y_amount }, duration, Phaser.Easing.Linear.None, true, delay, -1);
			this._particlePool.push(newPart);
			this.add(newPart);
		}
	}

	public updateColor(p_color: number) {
		for (let i=0; i < this._particlePool.length; i++) {
			this._particlePool[i].tint = p_color;
		}
	}

	public discardParticles() {
		for (let i=0; i < this._particlePool.length; i++) {
			this.game.tweens.removeFrom(this._particlePool[i]);
			this._particlePool[i].destroy();
		}
		this._particlePool = null;
	}
}