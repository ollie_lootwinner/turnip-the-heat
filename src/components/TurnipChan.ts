import { MiscUtil } from "../../slot-base/src/util/Misc";
import { SoundManager } from "../../slot-base/src/components/SoundManager";

export class TurnipChan extends Phaser.Image {

	private static xOffsets: number[] = [0.103, 0.083, 0.123];
	private static yOffsets: number[] = [0.27, 0.23, 0.23];
	private static angles: number[] = [0, -4, 4];

	private static xDestinations: number[] = [-0.25, 0.5, 1.25];
	private static yDestinations: number[] = [-0.25, 0.5, 1.25];

	private col: number;
	private row: number;
	private playerIndex: number;

	constructor(game: Phaser.Game, x: number, y: number, playerIndex: number = 0) {
		let xOffset: number = TurnipChan.xOffsets[playerIndex];
		let yOffset: number = TurnipChan.yOffsets[playerIndex];
		super(game, game.width * (0.13 * x + xOffset), game.height * (0.245 * y + yOffset), "game/background_texture", `turnip_${playerIndex}_idle_0`);

		this.col = x;
		this.row = y;
		this.playerIndex = playerIndex;
		this.anchor.set(0.5, 1.0);
		this.angle = TurnipChan.angles[playerIndex];

		this.animations.add(
			"idle", [`turnip_${playerIndex}_idle_0`, `turnip_${playerIndex}_idle_1`,
			`turnip_${playerIndex}_idle_2`, `turnip_${playerIndex}_idle_1`, `turnip_${playerIndex}_idle_0`]
		);
		this.animations.add(
			"blink", [`turnip_${playerIndex}_idle_blink_0`, `turnip_${playerIndex}_idle_blink_1`, `turnip_${playerIndex}_idle_blink_2`,
			`turnip_${playerIndex}_idle_blink_1`, `turnip_${playerIndex}_idle_blink_0`]
		);
		this.animations.add(
			"win", [`turnip_${playerIndex}_win_0`, `turnip_${playerIndex}_win_1`, `turnip_${playerIndex}_win_2`,
			`turnip_${playerIndex}_win_3`, `turnip_${playerIndex}_win_4`, `turnip_${playerIndex}_win_3`,
			`turnip_${playerIndex}_win_2`, `turnip_${playerIndex}_win_1`, `turnip_${playerIndex}_win_0`]
		);
		this.animations.add(
			"flying", [`turnip_${playerIndex}_flying_0`, `turnip_${playerIndex}_flying_1`,
			`turnip_${playerIndex}_flying_2`, `turnip_${playerIndex}_flying_1`, `turnip_${playerIndex}_flying_0`]
		);
		this.animations.add(
			"cower", [`turnip_${playerIndex}_cower_0`, `turnip_${playerIndex}_cower_1`,
			`turnip_${playerIndex}_cower_2`, `turnip_${playerIndex}_cower_3`, `turnip_${playerIndex}_cower_0`]
		);
		this.animations.add(
			"adoreIntro", [`turnip_${playerIndex}_mesmerised_intro_0`,
			`turnip_${playerIndex}_mesmerised_intro_1`, `turnip_${playerIndex}_mesmerised_intro_2`]
		);
		this.animations.add(
			"adoreLoop", [`turnip_${playerIndex}_mesmerised_loop_0`, `turnip_${playerIndex}_mesmerised_loop_1`,
			`turnip_${playerIndex}_mesmerised_loop_2`, `turnip_${playerIndex}_mesmerised_loop_1`]
		);
	}

	public idle() {
		if (this.animations.currentAnim) { this.animations.currentAnim.stop(); }

		let idleAnim = this.animations.play("idle", 12, true);
		let reqLoops: number = 2 + Math.ceil(Math.random() * 3);
		let numLoops: number = 0;
		idleAnim.onLoop.add(() => {
			numLoops++;
			if (numLoops === reqLoops) {
				idleAnim.stop();
				this.blink();
			}
		});
	}

	public blink() {
		if (this.animations.currentAnim) { this.animations.currentAnim.stop(); }

		MiscUtil.animCompletePromise( this.animations.play("blink", 12, false) )
		.then(() => {
			this.idle();
		});
	}

	public cower() {
		if (this.animations.currentAnim) { this.animations.currentAnim.stop(); }

		MiscUtil.animCompletePromise( this.animations.play("cower", 12, false) )
		.then(() => {
			this.idle();
		});
	}

	public async fly() {
		if (this.animations.currentAnim) { this.animations.currentAnim.stop(); }

		this.animations.play("flying", 12, true);
		this.game.add.tween(this)
		.to({ angle: Math.floor(Math.random() * 360) }, 700, Phaser.Easing.Linear.None, true);

		let xIndex: number = Math.floor(Math.random() * TurnipChan.xDestinations.length);
		let destX: number = TurnipChan.xDestinations[xIndex];
		let yIndex: number = Math.floor(Math.random() * TurnipChan.yDestinations.length);
		if ( ((destX > 0 && destX < 1) || this.row === 1) && yIndex === 1 ) {
			yIndex = Math.random() < 0.5 ? 0 : 2;
		}
		let destY: number = TurnipChan.yDestinations[yIndex];

		let dest = new Phaser.Point(destX * this.game.width, destY * this.game.height);
		let duration: number = 2000 + Math.floor(Math.random() * 800);

		this.game.add.tween(this).to({ alpha: 0 }, duration * 0.5, Phaser.Easing.Linear.None, true, duration * 0.5);
		this.game.add.tween(this).to({ y: dest.y }, duration, Phaser.Easing.Back.Out, true);
		let xTween = this.game.add.tween(this)
		.to({ x: dest.x }, duration, Phaser.Easing.Back.Out, true);

		SoundManager.instance.playFile(Math.random() < 0.25 ? "sfx/squeak_0" : "sfx/squeak_1");

		await MiscUtil.tweenCompletePromise(xTween);
	}

	public async win() {
		if (this.animations.currentAnim) { this.animations.currentAnim.stop(); }

		this.animations.play("win", 12, true);
	}

	public async adore() {
		if (this.animations.currentAnim) { this.animations.currentAnim.stop(); }

		let intro = this.animations.play("adoreIntro", 12, false);
		intro.onComplete.addOnce(() => {
			this.animations.play("adoreLoop", 12, true);
		});
	}

	public resetToStart() {
		if (this.animations.currentAnim) { this.animations.currentAnim.stop(); }
		this.game.tweens.removeFrom(this);
		this.game.tweens.removeFrom(this.scale);
		this.visible = false;
		this.alpha = 1;
		this.angle = TurnipChan.angles[this.playerIndex];
		this.scale.y = 1;
		let xOffset: number = TurnipChan.xOffsets[this.playerIndex];
		let yOffset: number = TurnipChan.yOffsets[this.playerIndex];
		this.x = this.game.width * (0.13 * this.col + xOffset);
		this.y = this.game.height * (0.245 * this.row + yOffset);
	}
}