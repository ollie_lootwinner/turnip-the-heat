export class DividerBarAnimations {

	protected static _game: Phaser.Game;
	protected static stars: Phaser.Image[][] = [];

	public static init(p_game: Phaser.Game) {
		this._game = p_game;
		for(let i = 0; i < 3; i++) {
			this.stars.push([]);
		}
	}

	public static addStarToColumn(star: Phaser.Image, columnNum: number) {
		this.stars[columnNum].push(star);
	}

	public static spinStars() {

		let newAngle: number = 360;
		let delay: number = 150;
		let spinTime: number = 500;

		for(let i = 0; i < 9; i++) {
			this._game.add.tween(this.stars[1][i]).to( { angle: newAngle }, spinTime, Phaser.Easing.Linear.None, true, delay * i);
			this._game.add.tween(this.stars[2][i]).to( { angle: newAngle }, spinTime, Phaser.Easing.Linear.None, true, delay * i);

			this._game.add.tween(this.stars[1][i].scale).to( { x: 1.5, y: 1.5 }, spinTime, Phaser.Easing.Linear.None, true, delay * i)
			.onComplete.addOnce(() => {
				this._game.add.tween(this.stars[1][i].scale).to( { x: 1, y: 1 }, spinTime, Phaser.Easing.Linear.None, true);
			});

			this._game.add.tween(this.stars[2][i].scale).to( { x: 1.5, y: 1.5 }, spinTime, Phaser.Easing.Linear.None, true, delay * i)
			.onComplete.addOnce(() => {
				this._game.add.tween(this.stars[2][i].scale).to( { x: 1, y: 1 }, spinTime, Phaser.Easing.Linear.None, true);
			});
		}
	}
}