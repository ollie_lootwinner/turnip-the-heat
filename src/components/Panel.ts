import { MiscUtil } from "../../slot-base/src/util/Misc";
import { TurnipChan } from "./TurnipChan";
import { SoundManager } from "../../slot-base/src/components/SoundManager";

export class Panel extends Phaser.Group {

	private static LETTERS: string[] = [
		"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"
	];

	private _bounds: Phaser.Rectangle;

	private _x: number;
	public get gridX(): number { return this._x; }
	private _y: number;
	public get gridY(): number { return this._y; }

	private _value: number;
	public get value(): number { return this._value; }

	private _isPlayerSelected: boolean = false;
	public get selected(): boolean { return this._isPlayerSelected; }

	private _spot: Phaser.Image;
	public get currentPos(): Phaser.Point { return this._spot.position; }

	private _idPanel: Phaser.Image;
	private _idText: Phaser.BitmapText;

	private _turnipLayer: Phaser.Group;
	private _characters: TurnipChan[];
	private _explosion: Phaser.Image;
	private _scorchMark: Phaser.Image;
	private _winIconGroup: Phaser.Group;

	public init(bounds: Phaser.Rectangle, i: number, turnipLayer: Phaser.Group, onTap: Function) {
		this._bounds = bounds;
		this._x = i % 6;
		this._y = Math.floor(i / 6);
		this._value = i + 1;
		this._turnipLayer = turnipLayer;
		this._characters = [];
		for (let j = 2; j >= 0; j--) {
			let char = new TurnipChan(this.game, this._x, this._y, j);
			char.visible = false;
			this._turnipLayer.addChild(char);
			this._characters.unshift(char);
		}

		this._spot = new Phaser.Image(
			this.game, this._bounds.width * (0.13 * this._x + 0.103), this._bounds.height * (0.245 * this._y + 0.24), "game/background_texture", "pot_empty"
		);
		this._spot.anchor.set(0.5);
		this._spot.inputEnabled = true;
		this._spot.events.onInputUp.add( (obj: any, pointer: Phaser.Pointer) => {
			onTap(this, pointer);
		});
		this.addChild(this._spot);

		this._idPanel = new Phaser.Image(
			this.game, this._spot.x, this._spot.y + this._spot.height * 0.44, "game/background_texture", "panel_id"
		);
		this._idPanel.anchor.set(0.5);
		this.addChild(this._idPanel);

		this._idText = new Phaser.BitmapText(this.game, 0, 0, "fonts/font_standard", `${Panel.LETTERS[i]}`, 64, "center");
		this._idText.anchor.set(0.55, 0.4);
		this._idPanel.addChild(this._idText);
	}

	public setSelected(p_val: boolean, playerIndex: number) {
		if (playerIndex === 0) { this._isPlayerSelected = p_val; }

		this._characters[playerIndex].visible = p_val;
		this._characters[playerIndex].idle();

		SoundManager.instance.playFile(p_val ? "sfx/bet_valid" : "sfx/bet_invalid", 0.75);
	}

	public async reveal() {
		if (!this._explosion) {
			let pos = new Phaser.Point(this._spot.x, this._spot.y - this._spot.height * 0.15);
			this._explosion = new Phaser.Image(this.game, pos.x, pos.y, "game/background_texture", "explode_fx_0");
			this._explosion.anchor.set(0.5);
			this._explosion.animations.add(
				"explode", ["explode_fx_0", "explode_fx_1", "explode_fx_2", "explode_fx_3", "explode_fx_4", "explode_fx_5"]
			);
			this._turnipLayer.addChild(this._explosion);

			this._scorchMark = new Phaser.Image(this.game, pos.x, pos.y, "game/background_texture", "pot_exploded");
			this._scorchMark.anchor.set(0.5);
			this._turnipLayer.addChild(this._scorchMark);
		}
		this._explosion.visible = true;
		let explode = this._explosion.animations.play("explode", 15, false);
		explode.onComplete.addOnce(() => {
			this._explosion.visible = false;
			this._scorchMark.visible = true;
		});

		SoundManager.instance.playFile("sfx/explode");

		for (let i=0; i < this._characters.length; i++) {
			if (this._characters[i].visible) {
				await MiscUtil.timeoutPromise(this.game, 100);

				this._characters[i].fly().then(() => {
					this._characters[i].visible = false;
				});
			}
		}

		await MiscUtil.timeoutPromise(this.game, 500);
	}

	public async animate(winMulti: number, delay: number) {
		for (let i=0; i < this._characters.length; i++) {
			if (this._characters[i].visible) { this._characters[i].win(); }
		}

		await MiscUtil.timeoutPromise(this.game, 1250 + delay);

		let iconFrame: number = 0;
		let text: string = "WIN!";
		if (winMulti > 8) {
			iconFrame = 2;
			text = "GREAT WIN!";
		} else if (winMulti > 3) {
			iconFrame = 1;
			text = "NICE WIN!";
		}

		this._winIconGroup = new Phaser.Group(this.game, this._turnipLayer);
		this._winIconGroup.scale.set(0.8);
		this._winIconGroup.x = this._spot.x;
		this._winIconGroup.y = this._spot.y - this._spot.height * 0.6;

		let starburst = new Phaser.Image(this.game, 0, 0, "game/background_texture", "starburst");
		starburst.anchor.set(0.5);
		this._winIconGroup.addChild(starburst);

		let icon = new Phaser.Image(this.game, 0, 0, "game/background_texture", `win_icon_${iconFrame}`);
		icon.anchor.set(0.5);
		icon.scale.set(1.25);
		this._winIconGroup.addChild(icon);

		this.game.add.tween(starburst)
		.from({ angle: 360 }, 6400, Phaser.Easing.Linear.None, true, 0, -1);

		let floatDown = this.game.add.tween(this._winIconGroup)
		.from({ y: this._spot.y - this._spot.height * 0.9 }, 500, Phaser.Easing.Linear.None, true);

		this.game.add.tween(this._winIconGroup)
		.from({ alpha: 0 }, 250, Phaser.Easing.Linear.None, true);

		let winText = new Phaser.BitmapText(
			this.game, 0, icon.height * -0.2, "fonts/font_standard", text, 48, "center"
		);
		winText.anchor.set(0.5);
		this._winIconGroup.addChild(winText);

		let winMultiText = new Phaser.BitmapText(
			this.game, 0, icon.height * 0.1, "fonts/font_standard", `${winMulti}x`, 96, "center"
		);
		winMultiText.anchor.set(0.5);
		this._winIconGroup.addChild(winMultiText);

		SoundManager.instance.playFile("sfx/win");

		await MiscUtil.timeoutPromise(this.game, 150);
		for (let i=0; i < this._characters.length; i++) {
			if (this._characters[i].visible) { this._characters[i].adore(); }
		}

		await MiscUtil.tweenCompletePromise(floatDown);
		this.game.add.tween(this._winIconGroup)
		.to({ y: this._spot.y - this._spot.height * 0.54 }, 800, Phaser.Easing.Linear.None, true, 0, -1, true);
	}

	public cower() {
		for (let i=0; i < this._characters.length; i++) {
			if (this._characters[i].visible) { this._characters[i].cower(); }
		}
	}

	public resetToStart() {
		this.game.tweens.removeFrom(this._spot.scale);
		this.game.tweens.removeFrom(this._spot);

		this._isPlayerSelected = false;

		for (let i=0; i < this._characters.length; i++) {
			this._characters[i].resetToStart();
		}

		if (this._winIconGroup) {
			this._winIconGroup.destroy();
		}

		if (this._explosion) {
			this._explosion.visible = false;
			this._scorchMark.visible = false;
		}
	}
}