import { BackgroundParticles } from "./BackgroundParticles";

export class SideBarGroup extends Phaser.Group {

		private _width: number;
		private _height: number;

		constructor(game: Phaser.Game, p_bounds: Phaser.Rectangle, mirrored: boolean) {
			super(game);
			this.init(mirrored, p_bounds);
		}

		public init(p_mirrored: boolean, p_bounds: Phaser.Rectangle) {

			let colW: number = p_bounds.width * (290 / 1920);
			let halfHeight: number = p_bounds.height / 2;
			let innerBack;

			let addParticles = new BackgroundParticles(this.game, colW);
			addParticles.position.x = (-250 / 1920) * p_bounds.width;
			addParticles.position.y = p_bounds.height / 2;

			this.add(addParticles);
			if (p_mirrored) {
				this.scale.x = -1;
				innerBack = new Phaser.Image(this.game, 15, halfHeight, "game/background_texture", "reel_frame_right");
				innerBack.anchor.set(1, 0.5);
			} else {
				innerBack = new Phaser.Image(this.game, 15, halfHeight, "game/background_texture", "reel_frame_withlevelbar_left");
				innerBack.anchor.set(1, 0.5);
			}
			this.add(innerBack);  // USE THIS FOR THE OUTER FRAMES OF REEL
		}

		public get imageWidth(): number {
			return this._width;
		}

		public get imageHeight(): number {
			return this._height;
		}
	}