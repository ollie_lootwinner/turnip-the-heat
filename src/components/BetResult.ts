import { Bet } from "./BetUI";
import { MathUtil } from "../../slot-base/src/util/Math";

export class BetResult {

	public winMultipliers: number[];
	public winningSpots: number[];

	private PAYTABLE: number = 2;

	constructor(public bets: Bet[], forceWin: boolean = false) {

		this.winMultipliers = [];
		this.winningSpots = [];

		let allSpots: number[] = [];
		for (let i = 1; i <= 18; i++) {
			allSpots.push(i);
		}
		allSpots = MathUtil.shuffle(allSpots);

		let numWins: number = this.PAYTABLE === 2 ? Math.ceil(Math.random() * 3) : 3;
		for (let i=0; i < numWins; i++) {
			let multi: number = this.PAYTABLE === 2 ? this.getMultiplierOption2() : this.getMultiplier();
			this.winMultipliers.push(multi);
			this.winningSpots.push(allSpots.pop());
		}

		if (forceWin) {
			this.winningSpots.splice(0, 1, bets[0].location);
		}
	}

	private getMultiplier(): number {
		let randy: number = Math.random();

		if (randy < 0.10) {
			return 1;
		}
		else if (randy < 0.4) {
			return 2;
		}
		else if (randy < 0.7) {
			return 3;
		}
		else if (randy < 0.8) {
			return 4;
		}
		else if (randy < 0.88) {
			return 5;
		}
		else if (randy < 0.93) {
			return 6;
		}
		else if (randy < 0.965) {
			return 10;
		}
		else if (randy < 0.985) {
			return 15;
		}
		else if (randy < 0.995) {
			return 25;
		}
		else if (randy < 0.998) {
			return 50;
		}

		return 100;
	}

	private getMultiplierOption2(): number {
		let randy: number = Math.random();

		if (randy < 0.2) {
			return 2;
		}
		else if (randy < 0.4) {
			return 3;
		}
		else if (randy < 0.6) {
			return 4;
		}
		else if (randy < 0.72) {
			return 5;
		}
		else if (randy < 0.845) {
			return 6;
		}
		else if (randy < 0.935) {
			return 10;
		}
		else if (randy < 0.965) {
			return 15;
		}
		else if (randy < 0.985) {
			return 25;
		}
		else if (randy < 0.998) {
			return 50;
		}

		return 100;
	}
}