<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>5.4.0</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>phaser-json-hash</string>
        <key>textureFileName</key>
        <filename>background_texture.png</filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>7</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>4096</int>
            <key>height</key>
            <int>4096</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>background_texture.json</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <true/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../fonts/font_standard.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>159,128,317,256</rect>
                <key>scale9Paddings</key>
                <rect>159,128,317,256</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">bg_panel_2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>445,273,890,545</rect>
                <key>scale9Paddings</key>
                <rect>445,273,890,545</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">close_0.png</key>
            <key type="filename">close_1.png</key>
            <key type="filename">close_2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>126,37,253,74</rect>
                <key>scale9Paddings</key>
                <rect>126,37,253,74</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">current_players_panel.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>64,183,127,365</rect>
                <key>scale9Paddings</key>
                <rect>64,183,127,365</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">explode_fx_0.png</key>
            <key type="filename">explode_fx_1.png</key>
            <key type="filename">explode_fx_2.png</key>
            <key type="filename">explode_fx_3.png</key>
            <key type="filename">explode_fx_4.png</key>
            <key type="filename">explode_fx_5.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>113,113,225,225</rect>
                <key>scale9Paddings</key>
                <rect>113,113,225,225</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">game_title.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>219,190,438,381</rect>
                <key>scale9Paddings</key>
                <rect>219,190,438,381</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">gamestate_panel.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>60,60,119,119</rect>
                <key>scale9Paddings</key>
                <rect>60,60,119,119</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">howTo_0.png</key>
            <key type="filename">howTo_1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>36,36,72,72</rect>
                <key>scale9Paddings</key>
                <rect>36,36,72,72</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">howTo_2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>37,37,75,75</rect>
                <key>scale9Paddings</key>
                <rect>37,37,75,75</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">panel_id.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>35,26,69,51</rect>
                <key>scale9Paddings</key>
                <rect>35,26,69,51</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">play_0.png</key>
            <key type="filename">play_1.png</key>
            <key type="filename">play_2.png</key>
            <key type="filename">play_3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>85,85,169,169</rect>
                <key>scale9Paddings</key>
                <rect>85,85,169,169</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">pot_empty.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>61,65,121,129</rect>
                <key>scale9Paddings</key>
                <rect>61,65,121,129</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">pot_exploded.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>48,38,95,75</rect>
                <key>scale9Paddings</key>
                <rect>48,38,95,75</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">results_panel.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>375,255,750,509</rect>
                <key>scale9Paddings</key>
                <rect>375,255,750,509</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">results_panel_banner.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>273,46,545,93</rect>
                <key>scale9Paddings</key>
                <rect>273,46,545,93</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">results_panel_middle.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>99,149,197,297</rect>
                <key>scale9Paddings</key>
                <rect>99,149,197,297</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">starburst.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>125,125,250,250</rect>
                <key>scale9Paddings</key>
                <rect>125,125,250,250</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">tag_player_0.png</key>
            <key type="filename">tag_player_1.png</key>
            <key type="filename">tag_player_2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>80,44,159,88</rect>
                <key>scale9Paddings</key>
                <rect>80,44,159,88</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">transition_badge.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>0.5</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>250,250,500,500</rect>
                <key>scale9Paddings</key>
                <rect>250,250,500,500</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">transition_starburst.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>0.5</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>600,600,1200,1200</rect>
                <key>scale9Paddings</key>
                <rect>600,600,1200,1200</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">turnips/turnip_0_cower_0.png</key>
            <key type="filename">turnips/turnip_0_cower_1.png</key>
            <key type="filename">turnips/turnip_0_cower_2.png</key>
            <key type="filename">turnips/turnip_0_cower_3.png</key>
            <key type="filename">turnips/turnip_0_flying_0.png</key>
            <key type="filename">turnips/turnip_0_flying_1.png</key>
            <key type="filename">turnips/turnip_0_flying_2.png</key>
            <key type="filename">turnips/turnip_0_idle_0.png</key>
            <key type="filename">turnips/turnip_0_idle_1.png</key>
            <key type="filename">turnips/turnip_0_idle_2.png</key>
            <key type="filename">turnips/turnip_0_idle_blink_0.png</key>
            <key type="filename">turnips/turnip_0_idle_blink_1.png</key>
            <key type="filename">turnips/turnip_0_idle_blink_2.png</key>
            <key type="filename">turnips/turnip_1_cower_0.png</key>
            <key type="filename">turnips/turnip_1_cower_1.png</key>
            <key type="filename">turnips/turnip_1_cower_2.png</key>
            <key type="filename">turnips/turnip_1_cower_3.png</key>
            <key type="filename">turnips/turnip_1_flying_0.png</key>
            <key type="filename">turnips/turnip_1_flying_1.png</key>
            <key type="filename">turnips/turnip_1_flying_2.png</key>
            <key type="filename">turnips/turnip_1_idle_0.png</key>
            <key type="filename">turnips/turnip_1_idle_1.png</key>
            <key type="filename">turnips/turnip_1_idle_2.png</key>
            <key type="filename">turnips/turnip_1_idle_blink_0.png</key>
            <key type="filename">turnips/turnip_1_idle_blink_1.png</key>
            <key type="filename">turnips/turnip_1_idle_blink_2.png</key>
            <key type="filename">turnips/turnip_2_cower_0.png</key>
            <key type="filename">turnips/turnip_2_cower_1.png</key>
            <key type="filename">turnips/turnip_2_cower_2.png</key>
            <key type="filename">turnips/turnip_2_cower_3.png</key>
            <key type="filename">turnips/turnip_2_flying_0.png</key>
            <key type="filename">turnips/turnip_2_flying_1.png</key>
            <key type="filename">turnips/turnip_2_flying_2.png</key>
            <key type="filename">turnips/turnip_2_idle_0.png</key>
            <key type="filename">turnips/turnip_2_idle_1.png</key>
            <key type="filename">turnips/turnip_2_idle_2.png</key>
            <key type="filename">turnips/turnip_2_idle_blink_0.png</key>
            <key type="filename">turnips/turnip_2_idle_blink_1.png</key>
            <key type="filename">turnips/turnip_2_idle_blink_2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>0.7</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>71,68,143,135</rect>
                <key>scale9Paddings</key>
                <rect>71,68,143,135</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">turnips/turnip_0_mesmerised_intro_0.png</key>
            <key type="filename">turnips/turnip_0_mesmerised_intro_1.png</key>
            <key type="filename">turnips/turnip_0_mesmerised_intro_2.png</key>
            <key type="filename">turnips/turnip_0_mesmerised_loop_0.png</key>
            <key type="filename">turnips/turnip_0_mesmerised_loop_1.png</key>
            <key type="filename">turnips/turnip_0_mesmerised_loop_2.png</key>
            <key type="filename">turnips/turnip_1_mesmerised_intro_0.png</key>
            <key type="filename">turnips/turnip_1_mesmerised_intro_1.png</key>
            <key type="filename">turnips/turnip_1_mesmerised_intro_2.png</key>
            <key type="filename">turnips/turnip_1_mesmerised_loop_0.png</key>
            <key type="filename">turnips/turnip_1_mesmerised_loop_1.png</key>
            <key type="filename">turnips/turnip_1_mesmerised_loop_2.png</key>
            <key type="filename">turnips/turnip_2_mesmerised_intro_0.png</key>
            <key type="filename">turnips/turnip_2_mesmerised_intro_1.png</key>
            <key type="filename">turnips/turnip_2_mesmerised_intro_2.png</key>
            <key type="filename">turnips/turnip_2_mesmerised_loop_0.png</key>
            <key type="filename">turnips/turnip_2_mesmerised_loop_1.png</key>
            <key type="filename">turnips/turnip_2_mesmerised_loop_2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>0.7</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>71,70,143,140</rect>
                <key>scale9Paddings</key>
                <rect>71,70,143,140</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">turnips/turnip_0_win_0.png</key>
            <key type="filename">turnips/turnip_0_win_1.png</key>
            <key type="filename">turnips/turnip_0_win_2.png</key>
            <key type="filename">turnips/turnip_0_win_3.png</key>
            <key type="filename">turnips/turnip_0_win_4.png</key>
            <key type="filename">turnips/turnip_1_win_0.png</key>
            <key type="filename">turnips/turnip_1_win_1.png</key>
            <key type="filename">turnips/turnip_1_win_2.png</key>
            <key type="filename">turnips/turnip_1_win_3.png</key>
            <key type="filename">turnips/turnip_1_win_4.png</key>
            <key type="filename">turnips/turnip_2_win_0.png</key>
            <key type="filename">turnips/turnip_2_win_1.png</key>
            <key type="filename">turnips/turnip_2_win_2.png</key>
            <key type="filename">turnips/turnip_2_win_3.png</key>
            <key type="filename">turnips/turnip_2_win_4.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>0.7</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>71,85,143,170</rect>
                <key>scale9Paddings</key>
                <rect>71,85,143,170</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">win_icon_0.png</key>
            <key type="filename">win_icon_1.png</key>
            <key type="filename">win_icon_2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>0.7</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>105,65,210,130</rect>
                <key>scale9Paddings</key>
                <rect>105,65,210,130</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>howTo_0.png</filename>
            <filename>howTo_1.png</filename>
            <filename>pot_empty.png</filename>
            <filename>pot_exploded.png</filename>
            <filename>explode_fx_0.png</filename>
            <filename>explode_fx_1.png</filename>
            <filename>explode_fx_2.png</filename>
            <filename>explode_fx_3.png</filename>
            <filename>explode_fx_4.png</filename>
            <filename>explode_fx_5.png</filename>
            <filename>win_icon_0.png</filename>
            <filename>win_icon_1.png</filename>
            <filename>win_icon_2.png</filename>
            <filename>turnips/turnip_0_cower_0.png</filename>
            <filename>turnips/turnip_0_cower_1.png</filename>
            <filename>turnips/turnip_0_cower_2.png</filename>
            <filename>turnips/turnip_0_cower_3.png</filename>
            <filename>turnips/turnip_0_flying_0.png</filename>
            <filename>turnips/turnip_0_flying_1.png</filename>
            <filename>turnips/turnip_0_flying_2.png</filename>
            <filename>turnips/turnip_0_idle_0.png</filename>
            <filename>turnips/turnip_0_idle_1.png</filename>
            <filename>turnips/turnip_0_idle_2.png</filename>
            <filename>turnips/turnip_0_idle_blink_0.png</filename>
            <filename>turnips/turnip_0_idle_blink_1.png</filename>
            <filename>turnips/turnip_0_idle_blink_2.png</filename>
            <filename>turnips/turnip_0_win_0.png</filename>
            <filename>turnips/turnip_0_win_1.png</filename>
            <filename>turnips/turnip_0_win_2.png</filename>
            <filename>turnips/turnip_0_win_3.png</filename>
            <filename>turnips/turnip_0_win_4.png</filename>
            <filename>turnips/turnip_1_cower_0.png</filename>
            <filename>turnips/turnip_1_cower_1.png</filename>
            <filename>turnips/turnip_1_cower_2.png</filename>
            <filename>turnips/turnip_1_cower_3.png</filename>
            <filename>turnips/turnip_1_flying_0.png</filename>
            <filename>turnips/turnip_1_flying_1.png</filename>
            <filename>turnips/turnip_1_flying_2.png</filename>
            <filename>turnips/turnip_1_idle_0.png</filename>
            <filename>turnips/turnip_1_idle_1.png</filename>
            <filename>turnips/turnip_1_idle_2.png</filename>
            <filename>turnips/turnip_1_idle_blink_0.png</filename>
            <filename>turnips/turnip_1_idle_blink_1.png</filename>
            <filename>turnips/turnip_1_idle_blink_2.png</filename>
            <filename>turnips/turnip_1_win_0.png</filename>
            <filename>turnips/turnip_1_win_1.png</filename>
            <filename>turnips/turnip_1_win_2.png</filename>
            <filename>turnips/turnip_1_win_3.png</filename>
            <filename>turnips/turnip_1_win_4.png</filename>
            <filename>../fonts/font_standard.png</filename>
            <filename>turnips/turnip_2_cower_0.png</filename>
            <filename>turnips/turnip_2_cower_1.png</filename>
            <filename>turnips/turnip_2_cower_2.png</filename>
            <filename>turnips/turnip_2_cower_3.png</filename>
            <filename>turnips/turnip_2_flying_0.png</filename>
            <filename>turnips/turnip_2_flying_1.png</filename>
            <filename>turnips/turnip_2_flying_2.png</filename>
            <filename>turnips/turnip_2_idle_0.png</filename>
            <filename>turnips/turnip_2_idle_1.png</filename>
            <filename>turnips/turnip_2_idle_2.png</filename>
            <filename>turnips/turnip_2_idle_blink_0.png</filename>
            <filename>turnips/turnip_2_idle_blink_1.png</filename>
            <filename>turnips/turnip_2_idle_blink_2.png</filename>
            <filename>turnips/turnip_2_win_0.png</filename>
            <filename>turnips/turnip_2_win_1.png</filename>
            <filename>turnips/turnip_2_win_2.png</filename>
            <filename>turnips/turnip_2_win_3.png</filename>
            <filename>turnips/turnip_2_win_4.png</filename>
            <filename>panel_id.png</filename>
            <filename>turnips/turnip_1_mesmerised_intro_0.png</filename>
            <filename>turnips/turnip_1_mesmerised_intro_1.png</filename>
            <filename>turnips/turnip_1_mesmerised_intro_2.png</filename>
            <filename>turnips/turnip_1_mesmerised_loop_0.png</filename>
            <filename>turnips/turnip_1_mesmerised_loop_1.png</filename>
            <filename>turnips/turnip_1_mesmerised_loop_2.png</filename>
            <filename>turnips/turnip_2_mesmerised_intro_0.png</filename>
            <filename>turnips/turnip_2_mesmerised_intro_1.png</filename>
            <filename>turnips/turnip_2_mesmerised_intro_2.png</filename>
            <filename>turnips/turnip_2_mesmerised_loop_0.png</filename>
            <filename>turnips/turnip_2_mesmerised_loop_1.png</filename>
            <filename>turnips/turnip_2_mesmerised_loop_2.png</filename>
            <filename>turnips/turnip_0_mesmerised_intro_0.png</filename>
            <filename>turnips/turnip_0_mesmerised_intro_1.png</filename>
            <filename>turnips/turnip_0_mesmerised_intro_2.png</filename>
            <filename>turnips/turnip_0_mesmerised_loop_0.png</filename>
            <filename>turnips/turnip_0_mesmerised_loop_1.png</filename>
            <filename>turnips/turnip_0_mesmerised_loop_2.png</filename>
            <filename>starburst.png</filename>
            <filename>current_players_panel.png</filename>
            <filename>gamestate_panel.png</filename>
            <filename>tag_player_0.png</filename>
            <filename>tag_player_1.png</filename>
            <filename>tag_player_2.png</filename>
            <filename>play_0.png</filename>
            <filename>play_1.png</filename>
            <filename>play_2.png</filename>
            <filename>play_3.png</filename>
            <filename>howTo_2.png</filename>
            <filename>results_panel.png</filename>
            <filename>results_panel_banner.png</filename>
            <filename>results_panel_middle.png</filename>
            <filename>close_0.png</filename>
            <filename>close_1.png</filename>
            <filename>close_2.png</filename>
            <filename>bg_panel_2.png</filename>
            <filename>game_title.png</filename>
            <filename>transition_badge.png</filename>
            <filename>transition_starburst.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
